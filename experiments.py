# -*- coding: utf-8 -*-
#   awgcontrol
#   Copyright (C) 2017 - 2019  Stefan Weichselbaumer
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
This module contains high-level functions which generate a complete pulse
sequence for various pEPR experiments, e.g. a Hahn echo sequence.
"""
import logging
import numpy as np

from sequence import Sequence
from waveforms.basic import Gaussian, Constant, Sine

def round(t, frequency):
    """Rounds time `t` to next multiple of frequency time interval."""
    if frequency > 0:
        return np.floor(t * frequency) / frequency
    return t

def pause(seq, pause, sample_rate):
    """Append a pause segment to `seq`. If pause is too long to be played by a
    single idle segment, this function automatically generates a waveform to
    extend the pause.

    Parameters
    ----------
    - seq         : The sequence to append to
    - pause       : The pause time (in seconds!)
    - sample_rate : The sample rate of the AWG
    """
    # The minimum delay is 10*64 samples
    min_idle_pause = 10*64/sample_rate
    if pause < min_idle_pause:
        raise ValueError("Pause is too short: {:.2f}us, sample_rate={:.0f} GHz, "
                "min_idle_pause={:.2f}us".format(pause*1e6, sample_rate/1e9,
                                                 min_idle_pause*1e6))
        #pause = min_idle_pause

#        print("Pause is too short!")
#        pause = min_idle_pause
    # The maximum duration of a idle segment is 64*2^25 samples. If the
    # requested pause time is shorter, we can just append the pause.
    # Otherwise, we have to insert Constant waveform segments to achieve the
    # pause.
    max_idle_pause = 64*2**25/sample_rate
    if pause < max_idle_pause:
        seq.append_pause(pause)
        return seq

    # The length of the constant waveform segment in between. Rounded to match
    # the 64 sample granularity.
    max_seg_pause = np.ceil(10e-6 * sample_rate / 64) * 64 / sample_rate
    # The constant wavform segment which is used to extend the pause
    pause_segment = Constant(pulse_length=max_seg_pause, sample=0)
    # Maximum number of waveform segment iterations
    max_loop_count = 2**32 - 1
    pause_remaining = pause

    # Loop as long as pause_remaining is longer than minimum pause time
    while pause_remaining > min_idle_pause:
        # First, append a normal pause. We subtract 1.5*max_seg_pause here
        # because we need at least on pause_segment after the idle segment
        # (since two consecutive idle segments are forbidden by the sequencer)
        pause = min(max_idle_pause - 1.5*max_seg_pause, pause_remaining)
        seq.append_pause(pause)
        pause_remaining -= pause

        # If remaining time is greater than one pause_segment, we append as
        # many pause segments as possible. However, we subtract max_seg_pause
        # from the remaining time to make sure that we always end with a idle
        # segment (which is then added in the next loop iteration)
        if pause_remaining > max_seg_pause:
            loop_count = int(min((pause_remaining - max_seg_pause)/max_seg_pause,
                                 max_loop_count))
            seq.append_waveform(pause_segment, loop_count=loop_count)
            pause_remaining -= loop_count*max_seg_pause

    return seq


def hahn_echo(frequency, sample_rate, Tpihalf, Tpi, Tau, Tacq, Apihalf=1.0,
              Api=1.0, phaseshift=90, shape='gaussian', averages=1, srt=1,
              record_pulses=False, seq=None):
    """Generate a Hahn echo sequence.

    Parameters
    ----------
    - frequency  : The frequency of the carrier/IF signal
    - Tpihalf    : Length of the pi half pulse
    - Tpi        : Length of the pi pulse
    - Tau        : Time between pulses
    - Tacq       : Length of acquisition window
    - Apihalf    : Amplitude of the pi half pulse (default: 1)
    - Api        : Amplitude of the pi pulse (default: 1)
    - phaseshift : Phase shift between pi half and pi pulse (default: 90 deg)
    - shape      : Pulse shape (gaussian or rect, default: gaussian)
    - averages   : Number of averages, ie. how often the pulse sequence is
                   repeated
    - srt        : The shot repetition rate, ie. wait time between consecutive
                   pulse sequences
    - record_pulses : Set to True if you want to record the pulses
    - seq        : Append to existing sequence (default: None)
    """
    log = logging.getLogger('HahnEcho')
    log.debug('Creating Hahn echo: {:.1f}us - {:.1f}us - {:.1f}us, '
              'Acq. Time={:.1f}us, Amplitudes={:.1f}, {:.1f}, '
              'phaseshift={:.0f}'.format(
                  Tpihalf*1e6, Tau*1e6, Tpi*1e6, Tacq*1e6, Api, Apihalf,
                  phaseshift))

    if shape == 'gaussian':
        # Create pulse shapes and calculate real tau and waiting time
        PulsePiHalf = Gaussian(frequency=frequency, Tp=Tpihalf, amplitude=Apihalf, phase=0)
        PulsePi     = Gaussian(frequency=frequency, Tp=Tpi, amplitude=Api,
                               phase=phaseshift)

        # Make sure that the length of the waveforms is a multiple of
        # 1/frequency.
        PulsePiHalf.time = round(PulsePiHalf.time, frequency)
        PulsePi.time     = round(PulsePi.time, frequency)
                               
        LengthPiHalf = PulsePiHalf.time
        LengthPi     = PulsePi.time
        log.debug("LengthPiHalf: {}, LengthPi: {}".format(LengthPiHalf, LengthPi))
        # Real wait time between pulses
        TauReal = round(Tau - 0.5*(LengthPiHalf + LengthPi), frequency)
        # Time between Pi pulse and acqusition start
        TWait   = Tau - 0.5*(Tacq + LengthPi)
        if TWait < LengthPi:
            TWait = 0.5*TauReal
        TWait = round(TWait, frequency)
    elif shape == 'rect':
        if frequency > 0:
            PulsePiHalf = Sine(frequency=frequency, periods=int(Tpihalf*frequency), phase=0)
            PulsePi     = Sine(frequency=frequency, periods=int(Tpi*frequency),
                               phase=phaseshift)
            LengthPiHalf = PulsePiHalf.time
            LengthPi     = PulsePi.time
            TauReal      = TWait = round(Tau, frequency)
        else:
            PulsePiHalf = Constant(pulse_length=Tpihalf)
            component = 'Q' if phaseshift == 90 else 'I'
            PulsePi     = Constant(pulse_length=Tpi, component=component)
            TauReal = TWait = Tau
    else:
        raise ValueError("Unknown shape type: {}".format(shape))

    LengthAcq   = round(480 / sample_rate, frequency)
    PulseAcq    = Constant(pulse_length=LengthAcq, sample=0)

    TPauseBetween = round(srt - LengthPiHalf - TauReal - LengthPi - TWait - Tacq, frequency) 

    # Sample marker is connected to the digitizer card TRIG IN.
    # Sync marker is connected to the Receiver Box Switch Input.
    # To open the PIN switch, we send a short pulse on the sync channel at the beginning
    # of the acquisition window. To close it a gain, we send another pulse at the end of
    # the acquisition window.

    if record_pulses:
        PulsePiHalf.sample_marker = [0]
        PulsePiHalf.sync_marker   = [0]
    else:
        PulseAcq.sample_marker = [0]
        PulseAcq.sync_marker   = [0]

    # Close switch sync marker
    PulseAcq.sync_marker.append(LengthAcq)

    log.debug('TauReal: {}, TWait: {}, TPauseBetween: {}'.format(
        TauReal, TWait, TPauseBetween))

    # Create sequence
    seq = seq or Sequence(loop_count=averages)
    seq.append_waveform(PulsePiHalf)
    seq = pause(seq, TauReal, sample_rate)
    seq.append_waveform(PulsePi)
    seq = pause(seq, TWait, sample_rate)
    seq.append_waveform(PulseAcq)
    if averages > 1:
        seq = pause(seq, TPauseBetween, sample_rate)
    
    return seq


def inv_recovery(frequency, sample_rate, Tpihalf, Tpi, T, Tau, Tacq,
        Apihalf=1.0, Api=1.0, phaseshift=90, shape='gaussian', averages=1,
        srt=1, record_pulses=False, seq=None):

    log = logging.getLogger('InvRec')
    log.debug('Creating inversion recovery: {:.1f}us - {:.1f}ms - {:.1f}us - {:.1f}us - {:.1f}us, '
              'Acq. Time={:.1f}us,Amplitudes={:.1f},{:.1f}'.format(
                  Tpi*1e6, T*1e3, Tpihalf*1e6, Tau*1e6, Tpi*1e6, Tacq*1e6,
                  Api, Apihalf))

    PulsePi     = Gaussian(frequency=frequency, Tp=Tpi, amplitude=Api, phase=90)
    PulsePiHalf = Gaussian(frequency=frequency, Tp=Tpihalf, amplitude=Apihalf, phase=90)

    # Make sure that the length of the waveforms is a multiple of
    # 1/frequency.
    PulsePiHalf.time = round(PulsePiHalf.time, frequency)
    PulsePi.time     = round(PulsePi.time, frequency)

    LengthPi     = PulsePi.time
    LengthPiHalf = PulsePiHalf.time

    TReal = round(T - 0.5*(LengthPi + LengthPiHalf), frequency)

    seq = Sequence(loop_count=averages)
    seq.append_waveform(PulsePi)
    seq = pause(seq, TReal, sample_rate)
    seq = hahn_echo(frequency, sample_rate, Tpihalf, Tpi, Tau, Tacq, Apihalf,
            Api, phaseshift, shape='gaussian', seq=seq, averages=averages,
            srt=srt, record_pulses=record_pulses)
    return seq

def sat_recovery(frequency, sample_rate, Tsat, T, Tpihalf, Tau, Tpi, Tacq,
        Asat=0.1, Apihalf=1.0, Api=1.0, phaseshift=0, shape='gaussian'):

    log = logging.getLogger('SatRec')
    log.debug('Creating saturation recovery: {:.1f}us - {:.1f}ms - {:.1f}us - {:.1f}us - {:.1f}us, '
              'Acq. Time={:.1f}us, Amplitudes={:.1f}, {:.1f},{:.1f}'.format(
                  Tsat*1e6, T*1e3, Tpihalf*1e6, Tau*1e6, Tpi*1e6, Tacq*1e6,
                  Asat, Api, Apihalf))

    PulseSat = Sine(frequency=frequency, periods=1, amplitude=Asat)
    seq = Sequence()
    seq.append_waveform(PulseSat, loop_count=frequency*Tsat)
    seq = pause(seq, T, sample_rate)
    seq = hahn_echo(frequency, sample_rate, Tpihalf, Tpi, Tau, Tacq, Apihalf,
            Api, phaseshift, shape=shape, seq=seq)
    return seq

