# -*- coding: utf-8 -*-
#   awgcontrol
#   Copyright (C) 2017 - 2019  Stefan Weichselbaumer
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
from __future__ import division, print_function, unicode_literals

import logging
import math
import os
import tempfile

from slave import transport

from lowlevel.driver import M8190A
from lowlevel.formatter import BINFormatter, TXTFormatter
from sequence import WaveformEntry, Scenario, Sequence
from waveforms import basic

class AWGController(object):
    """
    The AWGController class is used to provide a pythonic interface to control
    the AWG.

    It holds an instance of the `lowlevel.driver.M8190A` class to communicate
    with the hardware. It provides proxy methods so that the user does not
    have to interface with low-level functions.
    """

    def __init__(self, visa_addr, simulation=False):
        """Initialize the AWGController and connect to the hardware.
        
        Parameters
        ----------
        visa_addr  : The VISA resource string of the AWG
        simulation : Set to True to use a SimulatedTransport for development
        """
        #: Logger object
        self.log = logging.getLogger('AWG')
        #: The driver object
        self.driver = self.connect(visa_addr, simulation)

        self._amplitude = 0.7
        self._route = 'dac'
        self.reset()

    def connect(self, visa_addr, simulation):
        """Connect to the AWG and check identification."""
        if simulation:
            driver = M8190A(transport.SimulatedTransport())
        else:
            tsp = transport.Visa(visa_addr)
            # We need to set the timeout larger than the default value of 5s,
            # since the AWG takes quite some time for the first query response
            # after a *RST command.
            tsp._instrument.timeout = 30000
            driver = M8190A(tsp)
            idn = driver.identification
            if idn[1] != 'M8190A':
                raise RuntimeError("Unknown identification: {}".format(
                                   ','.join(idn)))
        self.log.info('Connected to M8190A')
        return driver

    def initialize_defaults(self):
        """Set some of the default settings."""
        self.driver.source.rosc.source      = 'axi'
        self.driver.source.frequency.source = 'internal'
        self.driver.instrument.coupling     = True

    @property
    def sample_rate(self):
        return self.driver.source.frequency.sample_rate

    @sample_rate.setter
    def sample_rate(self, value):
        self.driver.source.frequency.sample_rate = value

    @property
    def granularity(self):
        granularities = {'speed': 64, 'precision': 48,
                         'intx3': 24, 'intx12': 48,
                         'intx24': 24, 'intx24': 48}
        width = self.driver.trace[0].dwidth
        return granularities[width]

    def configure_trigger(self, level=None, impedance=None, slope=None,
                          source=None, frequency=None, mode=None):
        """Configure the trigger.

        Parameters
        ----------
        level     : Trigger input threshold level (in V)
        impedance : Trigger impedance (`high`: 1 kOhm, `low`: 50 Ohm)
        slope     : Trigger on `rising`, `falling` or `either` edges.
        source    : Trigger source (`internal` or `external`)
        frequency : Internal trigger frequency
        mode      : Trigger mode (continuous, triggered, gated)
        """
        self.log.info('Configuring trigger')
        trigger           = self.driver.trigger
        if level:
            trigger.level     = level
        if impedance:
            trigger.impedance = impedance
        if slope:
            trigger.slope     = slope
        if source:
            trigger.source    = source
        if frequency:
            trigger.frequency = frequency
        if mode:
            trigger.mode      = mode

    def configure_event(self, level=None, impedance=None, slope=None):
        """Configure the EVENT input.

        Parameters
        ----------
        level     : Event input threshold level (in V)
        impedance : Event input impedance (`high`: 1kOhm, `low`: 50 Ohm)
        slope     : Event on `rising`, `falling` or `either`
        """
        self.log.info('Configuring event system')
        event = self.driver.event
        if level:
            event.level = level
        if impedance:
            event.impedance = impedance
        if slope:
            event.slope = slope

    def configure_marker(self, sample_low=None, sample_high=None,
                         sync_low=None, sync_high=None,
                         channels=(0, 1)):
        """Configure the sample and sync markers.

        Parameters
        ----------
        sample_high : High level of the sample marker
        sample_low  : Low level of the sample marker
        sync_high   : High level of the sync marker
        sync_low    : Low level of the sync marker
        channels    : Tuple with channel numbers to set
        """
        self.log.info('Configuring marker')
        for ch in channels:
            marker = self.driver.source.marker[ch]
            if not sample_high is None:
                marker.sample_high = sample_high
            if not sample_low is None:
                marker.sample_low    = sample_low
            if not sync_high is None:
                marker.sync_high   = sync_high
            if not sync_low is None:
                marker.sync_low      = sync_low

    def reset(self):
        """Abort signal generation and reset to default settings."""
        self.log.info('Aborting and resetting AWG')
        self.driver.abort()
        self.driver.reset()
        self.initialize_defaults()

    def _upload_waveforms(self, waveforms):
        """Upload waveforms to the AWG."""

        self.log.info('Starting waveform upload')
        waveform_ids = {}
        filenames    = []
        granularity  = self.granularity
        for waveform in waveforms:
            # Synthesize waveforms
            I, Q = waveform.get_iq(self.sample_rate, granularity)
            if len(I) != len(Q):
                raise RuntimeError("Traces have different lengths")
            for i, samples in enumerate([I, Q]):
                id = self.driver.trace[i].define(len(samples))
                old_id = waveform_ids.get(waveform, id)
                if id != old_id:
                    msg = "Waveform upload: I and Q have different segment IDs."
                    self.log.error(msg)
                    raise RuntimeError(msg)
                waveform_ids[waveform] = id
                # Save waveform to file
                formatter = BINFormatter('speed')
                fd, fn = tempfile.mkstemp('.bin', 'awgcontrol-')
                fp = os.fdopen(fd, 'wb')
                formatter.write(samples, fp)
                fp.close()
                # Upload to AWG and store temp. filename for deletion
                self.driver.trace[i].iqimport(id, fn, 'bin')
                filenames.append(fn)
                self.log.info('Uploaded waveform (id={}, channel={})'
                              .format(id, i))

        if self.driver.operation_complete:
            for fn in filenames:
                os.remove(fn)

        self.log.info('Waveform upload complete.')

        return waveform_ids

    def _extract_waveforms(self, sequences):
        """Extracts all waveforms from the given list of sequences."""
        waveforms = []
        for sequence in sequences:
            for entry in sequence.entries:
                if isinstance(entry, WaveformEntry):
                    wf = entry.waveform
                    waveforms.append(wf)
        return waveforms

    def _upload_segments(self, segments):
        """Transfer a list of segments to the AWG."""
        self.log.info('Starting segments upload')
        for ch in [0, 1]:
            sequencer = self.driver.sequencer[ch]
            # Clear current sequence table
            self.driver.sequencer[ch].reset()
            # Write segments to sequencer table
            for i, segment in enumerate(segments):
                sequencer.write(i, segment.get_data())
        self.log.info('Segment upload complete.')

    def _reset_waveform_table(self):
        """Resets the waveform table."""
        self.log.info('Resetting waveform table')
        for ch in [0, 1]:
            # Delete all traces
            self.driver.trace[ch].delete_all()
            self.driver.trace[ch].dwidth = 'speed'
            self._granularity = None

    def run_arbitrary(self, waveform):
        """Run AWG in arbitrary mode and generate a single waveform.
        """
        self.log.info("Configuring for ARBitrary mode (single waveform)")
        self._reset_waveform_table()
        waveform_id = self._upload_waveforms([waveform])[waveform]
        for i in [0, 1]:
            self.driver.source.func[i] = 'arbitrary'
            self.driver.trace[i].selected_segment = waveform_id

    def run_sequence(self, sequence):
        """Run AWG in sequence mode and run a single sequence.
        """
        self.log.info("Configuring for sequence mode")
        self._reset_waveform_table()
        waveforms = self._extract_waveforms([sequence])
        waveform_ids = self._upload_waveforms(waveforms)
        segments = sequence.segments(self.sample_rate, waveform_ids)
        self._upload_segments(segments)
        self.driver.source.func[0] = 'sequence'
        self.driver.source.func[1] = 'sequence'

    def run_scenario(self, scenario):
        """Run AWG in scenario mode and run a single scenario.
        """
        self.log.info('Configuring for scenario mode')
        for i in [0, 1]:
            sequencer = self.driver.sequencer[i]
            sequencer.scenario_advance   = scenario.adv_mode
            sequencer.scenario_loopcount = scenario.loop_count
            sequencer.selected_scenario  = scenario.start
            sequencer.dynamic            = False
            self.driver.source.func[i] = 'scenario'
            
    def set_output(self, route='dac', amplitude=0.7):
        self._route = route
        self._amplitude = amplitude
    
    def start_generation(self, route=None, format='dnrz', amplitude=None):
        """Start signal generation.
        
        Parameters
        ----------
        route : The output route (default: 'dac')
        format: The output format (default: 'dnrz')
        """
        print(amplitude)
        for i in [0, 1]:
            self.driver.source.voltage[i].amplitude = amplitude or self._amplitude
            output = self.driver.output[i]
            output.route      = route or self._route
            output.dac_format = format
            output.state      = True
            
        self.driver.initiate()
        self.log.info('Signal generation started')


