# -*- coding: utf-8 -*-
#   awgcontrol
#   Copyright (C) 2017 - 2019  Stefan Weichselbaumer
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
import struct

"""The classes in the formatter module are used to save waveforms in different
formats to the disk.

Currently, we only support the ASCII based TXT format and the binary BIN
format. For more details, refer to pp. 287 and following of the M8190A manual.
"""

class Formatter(object):
    """Base class for all formatters. A formatter takes a list of samples and
    saves it to a certain data format which the waveform generator
    understands.
    """
    file_ext = ''

    def write(self, samples, fp):
        """ Writes the samples to a file. samples is a list of sample_tuple
        tuples.
        """
        raise NotImplementedError('This should be implemented in a subclass')

class TXTFormatter(Formatter):
    """Saves waveform data in TXT format.

    In the TXT format each sample is represented by a normalized value between
    -1.0 and 1.0. The sync and sample markers are indicated by ones in the
    second resp. third column.
    """
    file_ext = 'txt'

    def write(self, samples, fp):
        for sample in samples:
            fp.write("{0:6f},{1:1d},{2:1d}\r\n".format(
                     sample.value, sample.sync, sample.sample))


class CSVFormatter(Formatter):
    """Saves waveform data in CSV format.

    Similar to the TXT file, the values are saved as normalized values between
    -1 and 1. Additionally, a CSV file can contain a file header, e.g. to
    specify the sample rate.
    """
    file_ext = 'csv'

    def __init__(self, sample_rate=None):
        self.sample_rate = sample_rate

    def write(self, samples, fp):
        # Write sample rate if specified
        if self.sample_rate:
            fp.write('SampleRate = {0:e}\r\n'.format(self.sample_rate))
            fp.write('SetConfig = true\r\n')
        # Write data header
        fp.write('Y1,SyncMarker1,SampleMarker1\r\n')
        # Write samples
        for sample in samples:
            fp.write('{0:6f},{1:1d},{2:1d}\r\n'.format(
                sample.value, sample.sync, sample.sample))


class BINFormatter(Formatter):
    """Saves waveform data in the recommended binary data format."""
    file_ext = 'bin'

    def __init__(self, mode='speed'):
        """Initialize the BINFormatter.

        ``mode`` can be either ``'speed'`` (12-bit resolution) or
        ``'precision'`` (14-bit resolution).
        """
        super(BINFormatter, self).__init__()
        if not mode in ('speed', 'precision'):
            raise ValueError("Unknown mode: {0:s}".format(mode))

        self.bits = 12 if mode == 'speed' else 14
        self.mult = 2**(self.bits-1) - 1

    def write(self, samples, fp):
        for sample in samples:
            # Shift value to bit 16 - 2/4 (depending on self.bits)
            value = int(sample.value*self.mult) << (16 - self.bits)
            # toggle the sync marker and sample marker
            value ^= (-sample.sync   ^ value) & (1 << 1)
            value ^= (-sample.sample ^ value) & (1 << 0)
            fp.write(struct.pack('<h', value))

