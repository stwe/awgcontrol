# -*- coding: utf-8 -*-
#   awgcontrol
#   Copyright (C) 2017 - 2019  Stefan Weichselbaumer
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""The driver module implements a programmable interface for the Keysight
M8190A Arbitrary Waveform Generator.
"""
import os

from slave.driver import Command, Driver, CommandSequence, _dump
from slave.iec60488 import IEC60488, ObjectIdentification, Learn
from slave.types import Boolean, Float, Integer, Mapping, String, Stream


class M8190A(IEC60488, ObjectIdentification, Learn):
    """The Keysight M8190A Arbitrary Waveform Generator.
    
    Attributes
    ----------
    identification : The identification string of the instrument.
    operation_complete : True if the last command was completed (blocking).

    Subsystems
    ----------
    trigger, event, instrument, output, source, sequencer[i], trace[i], system

    Functions
    ---------
    reset : Resets the device to default settings.
    abort : Stop signal generation.
    initiate : Start signal generation.
    """

    def __init__(self, transport, protocol=None):
        super(M8190A, self).__init__(transport, protocol)

        self.trigger = Trigger(self._transport, self._protocol)
        self.event   = Event(self._transport, self._protocol)

        self.instrument = Instrument(self._transport, self._protocol)
        
        self.source = Source(self._transport, self._protocol)
        self.output = tuple(
                Output(i, self._transport, self._protocol)
                for i in [1, 2]
            )
        self.sequencer = tuple(
                SequencerTable(i, self._transport, self._protocol)
                for i in [1, 2]
            )
        self.trace = tuple(
                Trace(i, self._transport, self._protocol) for i in [1, 2]
            )

        self.system = System(self._transport, self._protocol)

    def clear(self):
        """Clear error queue."""
        self._write('*CLS')
    
    def abort(self):
        """Stop signal generation."""
        self._write(':ABOR')

    def initiate(self):
        """Start signal generation."""
        self._write(':INIT:IMM')


class Trigger(Driver):
    """The Trigger command layer.

    Attributes
    ----------
    mode      : Set or query the trigger mode ('continous', 'triggered', 'gated')
    level     : Set or query the trigger input threshold level.
    impedance : Set or query the trigger input impedance (high or low)
    slope     : Set or query the trigger input slope (rising, falling or either)
    source    : Set or query the source for the trigger function (internal or external)
    frequency : Set or query the frequency of the internal trigger generator
    enable_source  : Set or query the source for the enable event (trigger or event)
    advance_source : Set or query the source for the advancement event 
                     (trigger, event or internal)

    Functions
    ---------
    enable(channel) : Send the enable event to a channel.
    begin(channel)  : Send the start/begin event to a channel
    advance(channel): Send the advancement event to a channel
    """
    def __init__(self, transport, protocol=None):
        super(Trigger, self).__init__(transport, protocol)
        self.level = Command(':ARM:TRIG:LEV?', ':ARM:TRIG:LEV', Float)
        self.impedance = Command(':ARM:TRIG:IMP?', ':ARM:TRIG:IMP',
                Mapping({'low': 'LOW', 'high': 'HIGH'}))
        self.slope = Command(':ARM:TRIG:SLOP?', ':ARM:TRIG:SLOP',
                Mapping({'rising': 'POS', 'falling': 'NEG', 'either': 'EITH'}))
        self.source = Command(':ARM:TRIG:SOUR?', ':ARM:TRIG:SOUR',
                Mapping({'external': 'EXT', 'internal': 'INT'}))
        self.frequency = Command(':ARM:TRIG:FREQ?', ':ARM:TRIG:FREQ', Float)
        self.enable_source = Command(':TRIG:SOUR:ENAB?', ':TRIG:SOUR:ENAB',
                Mapping({'trigger': 'TRIG', 'event': 'EVEN'}))
        self.advance_source = Command(':TRIG:SOUR:ADV?', ':TRIG:SOUR:ADV',
                Mapping({'trigger': 'TRIG', 'event': 'EVEN', 'internal': 'INT'}))

    @property
    def mode(self):
        """Query the trigger mode"""
        cont = self._query((':INIT:CONT?', Boolean))
        if cont:
            return 'continuous'
        gate = self._query(('INIT:GATE?', Boolean))
        return 'gated' if gate else 'triggered'

    @mode.setter
    def mode(self, mode):
        """Set the trigger mode"""
        for ch in [1, 2]:
            if mode == 'continuous':
                self._write((':INIT:CONT{}'.format(ch), Boolean), True)
                self._write((':INIT:GATE{}'.format(ch), Boolean), False)
            elif mode == 'triggered':
                self._write((':INIT:CONT{}'.format(ch), Boolean), False)
                self._write((':INIT:GATE{}'.format(ch), Boolean), False)
            elif mode == 'gated':
                self._write((':INIT:CONT{}'.format(ch), Boolean), False)
                self._write((':INIT:GATE{}'.format(ch), Boolean), True)
            else:
                raise ValueError("Unknown trigger mode: {}".format(mode))
                break
        
    def enable(self, channel=1):
        """Send the enable event to a channel"""
        self._write(':TRIG:ENAB{}'.format(channel))

    def begin(self, channel=1):
        """Send the start/begin event to a channel"""
        self._write(':TRIG:BEG{}'.format(channel))

    def advance(self, channel=1):
        """Send the advancement event to a channel"""
        self._write(':TRIG:ADV{}'.format(channel))


class Event(Driver):
    """The Event command layer.

    Attributes
    ----------
    level     : Set or query the trigger input threshold level.
    impedance : Set or query the trigger input impedance (high or low)
    slope     : Set or query the trigger input slope (rising, falling or either)
    """
    def __init__(self, transport, protocol=None):
        super(Event, self).__init__(transport, protocol)
        self.level = Command(':ARM:EVEN:LEV?', 'ARM:EVEN:LEV', Float)
        self.impedance = Command(':ARM:EVEN:IMP?', ':ARM:EVEN:IMP',
                                 Mapping({'low': 'LOW', 'high': 'HIGH'}))
        self.slope = Command(':ARM:EVEN:SLOP?', ':ARM:EVEN:SLOP',
                             Mapping({'rising': 'POS', 'falling': 'NEG', 'either': 'EITH'}))


class Instrument(Driver):
    """The Instrument command layer.

    Attributes
    ----------
    coupling : Switch coupling on/off.
    """
    def __init__(self, transport, protocol=None):
        super(Instrument, self).__init__(transport, protocol)
        self.coupling = Command(':INST:COUP:STAT?', ':INST:COUP:STAT', Boolean)


class Output(Driver):
    """The Output command layer.

    Attributes
    ----------
    state[i]      : Turn the normal output on or off
    route[i]      : Select the output path (DAC, DC or AC)
    dac_format[i] : Set or query the DAC output format (dnrz, nrz, rz)
    """
    def __init__(self, ch, transport, protocol=None):
        super(Output, self).__init__(transport, protocol)
        self.channel = ch
        self.state = Command(':OUTP{}:NORM?'.format(ch),
                             ':OUTP{}:NORM'.format(ch),
                             Boolean)
        self.route = Command(':OUTP{}:ROUT?'.format(ch), 
                             ':OUTP{}:ROUT'.format(ch),
                             Mapping({'dac': 'DAC', 'dc': 'DC', 'ac': 'AC'}))
        self.dac_format = Command(':DAC{}:FORM?'.format(ch),
                                  ':DAC{}:FORM'.format(ch),
                                  Mapping({'dnrz': 'DNRZ', 'nrz': 'NRZ', 
                                           'rz': 'RZ'}))

class Source(Driver):
    """The Source command layer.

    Attributes
    ----------
    frequency  : Access to the Source -> Frequency command layer.
    rosc       : Access to the Source -> Reference Oscillator command layer.
    voltage[i] : Access to the Source -> Voltage command layer
    marker[i]  : Access to the Source -> marker command layer.
    func[i]    : Set or query the function (arbitrary, sequence or scenario)
    """
    def __init__(self, transport, protocol=None):
        super(Source, self).__init__(transport, protocol)
        self.frequency = SourceFrequency(self._transport, self._protocol)
        self.rosc = SourceROscillator(self._transport, self._protocol)
        self.voltage = tuple(
                SourceVoltage(i, self._transport, self._protocol)
                for i in [1, 2]
            )
        self.marker = tuple(
                SourceMarker(i, self._transport, self._protocol)
                for i in [1, 2]
            )
        self.func = CommandSequence(self._transport, self._protocol,
                (Command(':FUNC{}:MODE?'.format(i),
                         ':FUNC{}:MODE'.format(i),
                         Mapping({'arbitrary': 'ARB', 'sequence': 'STS',
                                  'scenario': 'STSC'})) for i in [1, 2]))

class SourceFrequency(Driver):
    """The Source -> Frequency command layer.

    Attributes
    ----------
    sample_rate : Set or query the sampling rate.
    external : Set or query the *external* sample frequency.
    source[i] : Turn on/off the external sample frequency source (channel 1).
    """
    def __init__(self, transport, protocol=None):
        super(SourceFrequency, self).__init__(transport, protocol)
        self.sample_rate = Command(':FREQ:RAST?', ':FREQ:RAST', Float)
        self.external = Command(':FREQ:RAST:EXT?', ':FREQ:RAST:EXT', Float)
        self.source = CommandSequence(self._transport, self._protocol,
                (Command(':FREQ:RAST:SOUR{}?'.format(idx), 
                        ':FREQ:RAST:SOUR{}'.format(idx),
                        Mapping({'internal': 'INT', 'external': 'EXT'}))
                for idx in [1, 2]))

class SourceROscillator(Driver):
    """The Source -> ReferenceOscillator command layer.

    Attributes
    ----------
    source : Set or query the reference clock source (external, axi or
             internal)
    frequency : Set or query the expected external reference clock frequency.
    
    Functions
    ---------
    check : Check if the internal, external or AXI oscillator input is
            present.
    """
    def __init__(self, transport, protocol=None):
        super(SourceROscillator, self).__init__(transport, protocol)
        self.source = Command(':ROSC:SOUR?', ':ROSC:SOUR',
                Mapping({'internal': 'INT', 'axi': 'AXI', 'external': 'EXT'}))
        self.frequency = Command(':ROSC:FREQ?', ':ROSC:FREQ', Float)

    def check(self, source):
        if not source in ('external', 'axi', 'internal'):
            raise ValueError("Unknown source: {:s}".format(source))
        return self._query((':ROSC:SOUR:CHEC?', Boolean, 
            Mapping({'internal': 'INT', 'external': 'EXT', 'axi': 'AXI'})),
            source)

class SourceVoltage(Driver):
    """The Source -> Voltage subsystem.

    Attributes
    ----------
    amplitude : Set or query the voltage amplitude (0.35 V – 0.7 V)
    offset    : Set or query the voltage offset (-0.02 V – 0.02 V)
    high      : Set or query the output HIGH level (0.155 V – 0.37 V)
    low       : Set or query the output LOW level (-0.37 V – -0.155 V)
    """
    def __init__(self, ch, transport, protocol=None):
        super(SourceVoltage, self).__init__(transport, protocol)
        self.amplitude = Command(':VOLT{}:AMPL?'.format(ch),
                                 ':VOLT{}:AMPL'.format(ch),
                                 Float(min=0.2, max=2))
        self.offset = Command(':VOLT{}:OFFS?'.format(ch),
                              ':VOLT{}:OFFS'.format(ch), 
                              Float(min=-0.02, max=0.02))
        self.high = Command(':VOLT{}:HIGH?'.format(ch),
                            ':VOLT{}:HIGH'.format(ch), 
                            Float(min=0.155, max=0.37))
        self.low = Command(':VOLT{}:LOW?'.format(ch),
                           ':VOLT{}:LOW'.format(ch), 
                           Float(min=-0.37, max=-0.155))

class SourceMarker(Driver):
    """The Source -> Marker subsystem.

    Attributes
    ----------
    sample_amplitude : Set or query the sample marker amplitude (0 – 2.25 V)
    sample_offset    : Set or query the sample marker offset (-0.5 – 1.75 V)
    sample_high      : Set or query the sample marker high level (0.5 - 1.75V)
    sample_low       : Set or query the sample marker low level (-0.5 - 1.75V)
    sync_amplitude   : Set or query the sync marker amplitude (0 – 2.25 V)
    sync_offset      : Set or query the sync marker offset (-0.5 – 1.75 V)
    sync_high       : Set or query the sync marker high level (0.5 - 1.75V)
    sync_low        : Set or query the sync marker low level (-0.5 - 1.75V)
    """
    def __init__(self, ch, transport, protocol=None):
        super(SourceMarker, self).__init__(transport, protocol)
        self.channel = ch
        self.sample_amplitude = Command(':MARK{}:SAMP:VOLT:AMPL?'.format(ch),
                                        ':MARK{}:SAMP:VOLT:AMPL'.format(ch),
                                        Float(min=0, max=2.25))
        self.sample_offset = Command(':MARK{}:SAMP:VOLT:OFFS?'.format(ch),
                                     ':MARK{}:SAMP:VOLT:OFFS'.format(ch),
                                     Float(min=-0.5, max=1.75))
        self.sample_high = Command(':MARK{}:SAMP:VOLT:HIGH?'.format(ch),
                                     ':MARK{}:SAMP:VOLT:HIGH'.format(ch),
                                     Float(min=0.5, max=1.75))
        self.sample_low = Command(':MARK{}:SAMP:VOLT:LOW?'.format(ch),
                                     ':MARK{}:SAMP:VOLT:LOW'.format(ch),
                                     Float(min=-0.5, max=1.75))
        self.sync_amplitude = Command(':MARK{}:SYNC:VOLT:AMPL?'.format(ch),
                                      ':MARK{}:SYNC:VOLT:AMPL'.format(ch),
                                      Float(min=0, max=2.25))
        self.sync_offset = Command(':MARK{}:SYNC:VOLT:OFFS?'.format(ch),
                                   ':MARK{}:SYNC:VOLT:OFFS'.format(ch),
                                   Float(min=-0.5, max=1.75))
        self.sync_high = Command(':MARK{}:SYNC:VOLT:HIGH?'.format(ch),
                                 ':MARK{}:SYNC:VOLT:HIGH'.format(ch),
                                 Float(min=0.5, max=1.75))
        self.sync_low = Command(':MARK{}:SYNC:VOLT:LOW?'.format(ch),
                                ':MARK{}:SYNC:VOLT:LOW'.format(ch),
                                Float(min=-0.5, max=1.75))

class SequencerTable(Driver):
    """The SequencerTable subsytem.

    Use this subsystem to manipulate the sequencer table to generate sequences
    and scenarios.

    Attributes
    ----------
    selected_sequence : Select where in the sequence table the sequence starts
                        in STSequence mode.
    dynamic : Enable or disable dynamic mode.
    dynamic_selected_sequence : Set or query the selected sequence table entry in dynamic
                                mode
    selected_scenario : Select where in the sequence table the sequence starts
                        in STScenario mode
    scenario_advance : Select the advancement mode for scenarios
    scenario_loopcount : Set or query the number of iterations of the current
                         scenario.

    Functions
    ---------
    reset: Reset the sequencer table.
    read:  Read entries from the sequencer table.
    write: Write directly to the sequencer table.

    """
    def __init__(self, ch, transport, protocol=None):
        super(SequencerTable, self).__init__(transport, protocol)
        self.channel = ch
        self.selected_sequence = Command(
                ':STAB{}:SEQ:SEL?'.format(self.channel),
                ':STAB{}:SEQ:SEL'.format(self.channel),
                Integer(0, 524287))
        self.dynamic = Command(
                ':STAB{}:DYN?'.format(self.channel),
                ':STAB{}:DYN'.format(self.channel),
                Boolean)
        self.dynamic_selected_sequence = Command(
                ':STAB{}:DYN:SEL?'.format(self.channel),
                ':STAB{}:DYN:SEL'.format(self.channel),
                Integer(0, 524287))
        self.selected_scenario = Command(
                ':STAB{}:SCEN:SEL?'.format(self.channel),
                ':STAB{}:SCEN:SEL'.format(self.channel),
                Integer(0, 524287))
        self.scenario_advance = Command(
                ':STAB{}:SCEN:ADV?'.format(self.channel),
                ':STAB{}:SCEN:ADV'.format(self.channel),
                Mapping({'auto': 'AUTO', 'conditional': 'COND',
                         'repeat': 'REP', 'single': 'SING'}))
        self.scenario_loopcount = Command(
                ':STAB{}:SCEN:COUN?'.format(self.channel),
                ':STAB{}:SCEN:COUN'.format(self.channel),
                Integer(min=1, max=2**32-1))

    def reset(self):
        self._write(':STAB{}:RES'.format(self.channel))

    def write(self, sequence_id, entries):
        """Write directly into the sequencer memory.
        
        Use this command to write entries into the sequencer table. The
        sequence_id parameter specifies at which entry to write the following
        entries. An entry can either be a waveform segment or an idle segment.
        Each entry consists of six int32 values. A waveform segment is
        described by the following six values:

        - control_parameter: This value contains various flags describing the
                             entry (i.e. start flag, advancement mode, type of
                             segment)
        - sequence_loop: The number of sequence iterations (can only be set
                         for the first segment in a sequence)
        - segment_loop: The number of segment iterations.
        - segment_id: The segment id to be played.
        - start_address: The address where the waveform segment is started
                         (usually 0 to play from the beginnging)
        - end_address: The address where the waveform segment is ended
                       (usually 0xFFFFFFFF to play until the end)


        An idle segment can be used to play a static DAC sample over an
        extended period of time. It is described by the following six values:

        - control_parameter: Describes the entry
        - sequence_loop: The number of sequence loops
        - command_code: Has to be set to zero for an idle segment.
        - idle_sample: The DAC value which should be played during the pause
        - idle_delay: The duration of the pause in samples (must obey
                      granularity)
        - reserved: Has to be set to zero.
        """
        if len(entries) % 6 != 0:
            raise ValueError("Each entries has to be specified by six "
                             "integers.")
        self._write((':STAB{}:DATA'.format(self.channel), 
                     Stream(Integer, Integer, Integer, 
                            Integer, Integer, Integer)),
                     sequence_id, *entries)

    def read(self, sequence_id, length):
        """Read from the sequencer memory. 

        sequence_id specifies where we should start reading. length describes
        how many entries we want to read.
        """
        return self._query((':STAB{}:DATA?'.format(self.channel),
                           Stream(Integer, Integer, Integer, 
                                  Integer, Integer, Integer),
                           [Integer, Integer]), sequence_id, length)

class Trace(Driver):
    """The Trace subsystem.

    Use this subsystem to generate arbitrary waveform segments and to load
    waveforms to the device.

    Attributes
    ----------
    dwidth : Set or query the waveform output mode (12 bit or 14 bit)
    selected_segment : Set or query the selected waveform segment

    Functions
    ---------
    delete(segment_id): Delete a segment.
    delete_all: Deletes all segments.
    define(length): Define a new waveform and return the ID.
    iqimport(…):        Import a waveform segment from file.
        Parameters to iqimport are:
        - segment_id: The ID of the newly created segment (1…512k-1)
        - file_name: The local file to read
        - file_type: The data format of the text file (See manual sec. 8.22.13)
        - data_type: Can be 'ionly', 'qonly' or 'both'
        - marker_flag: Set or ignore markers (default: True)
        - padding: 'alength' or 'fill'
        - init_valueI, init_valueQ: initialization values for I/Q
        - ignore_header: Ignore additional header specifications in the file
    """
    def __init__(self, ch, transport, protocol=None):
        super(Trace, self).__init__(transport, protocol)
        self.channel = ch
        self.dwidth = Command(':TRAC{}:DWID?'.format(ch),
                              ':TRAC{}:DWID'.format(ch),
                              Mapping({'speed': 'WSP', 'precision': 'WPR',
                                       'intx3': 'INTX3', 'intx12': 'INTX12',
                                       'intx24': 'INTX24', 'intx48': 'INTX48'}))
        self.selected_segment = Command(
                ':TRAC{}:SEL?'.format(self.channel),
                ':TRAC{}:SEL'.format(self.channel),
                Integer(0, 524287))

    def delete(self, segment_id):
        self._write((':TRAC{}:DEL'.format(self.channel), 
                     Integer(min=1, max=524287)), segment_id)

    def delete_all(self):
        self._write(':TRAC{}:DEL:ALL'.format(self.channel))

    def iqimport(self, segment_id, file_name, file_type, dtype='ionly',
            marker_flag=True, padding='alength', init_valueI=0, init_valueQ=0,
            ignore_header=False):

        types = [
            # segment_id, filename
            Integer(min=1, max=524287), String,
            # file type
            Mapping({'txt': 'TXT', 'txt14': 'TXT14', 'bin': 'BIN', 
                'iqbin':'IQBIN', 'bin6030': 'BIN6030', 'bin5110': 'BIN5110',
                'licensed': 'LIC', 'mat89600': 'MAT89600', 
                'dsa90000': 'DSA90000', 'csv': 'CSV'}),
            # data type
            Mapping({'ionly': 'IONL', 'qonly': 'QONL', 'both': 'BOTH'}),
            # marker_flag, padding
            Boolean, Mapping({'alength': 'ALEN', 'fill': 'FILL'}),
            # init_valueI, init_valueQ
            Integer(min=0, max=2**16), Integer(min=0, max=2**16),
            # ignore header parameters
            Boolean
        ]

        file_name = "'{}'".format(file_name)
        args = [segment_id, file_name, file_type, dtype, marker_flag, padding,
                init_valueI, init_valueQ, ignore_header]
        self._write((':TRAC{}:IQIM'.format(self.channel), types), *args)

    def define(self, length):
        """Define a new waveform with a specific length and return the
        assigned segment id.
        """
        return self._query((':TRAC{}:DEF:NEW?'.format(self.channel),
                            Integer(min=1, max=2**19), Integer),
                            length)


class System(Driver):
    """The System command layer.
    
    Attributes
    ----------
    error : The last error in the error queue.
    licenses : All installed licenses.

    Functions
    ---------
    error_list : Returns a list of all errors in the error queue.
    """

    def __init__(self, transport, protocol=None):
        super(System, self).__init__(transport, protocol)

        self.error = Command(('SYST:ERR?', [Integer, String]))
        self.licenses = Command(('SYST:LIC:EXT:LIST?', String))

    def error_list(self):
        """Returns a list of all errors."""
        errors = []
        while True:
            errno, desc = self.error
            if errno == 0:
                break
            errors.append((errno, desc))
        return errors

