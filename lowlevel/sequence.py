# -*- coding: utf-8 -*-
#   awgcontrol
#   Copyright (C) 2017 - 2019  Stefan Weichselbaumer
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
import copy
import os
import struct

from functools import partial

import numpy as np

"""The SequencerTable class is a low-level representation of the sequencer
table of the waveform generator. It holds a list of segments which can either
be IdleSegments or WaveformSegments. A segment in the sequencer table consists
of six Int32 values. The Segment class is used to abstract these values. The
meaning of the values depends on the type of segment.

See pp. 265 - 269 of the M8190A manual for more details about the sequencer
table.

This module allows you to programmatically create the sequence table. The
sequence table can then be transferred to the VNA using the slave driver.
"""

ADVANCEMENT_MODES = ['auto', 'conditional', 'repeat', 'single']

class SequencerTable(object):
    """The sequence table contains a list of segments."""

    def __init__(self):
        """Initialize a sequencer table.  """
        self.segments       = []

    def add_segment(self, segment):
        self.segments.append(segment)

    def save(self, fn):
        """Exports the sequence table to the specified file."""
        with open(fn, 'wb') as fp:
            for segment in self.segments:
                data = segment.get_data()
                bytes  = struct.pack('<IIIIII', *data)
                fp.write(bytes)

    def load(self, fn):
        """Import entries from the specified file."""
        with open(fn, 'rb') as fp:
            fp.seek(0, os.SEEK_END)
            filesize = fp.tell()
            fp.seek(0)

            if filesize % 24 != 0:
                raise RuntimeError("Invalid file size: {0:d}, should be"
                                   "multiple of 24".format(filesize))

            for chunk in iter(partial(fp.read, 24), ''):
                segment_data = struct.unpack('<IIIIII', chunk)
                seg = Segment.from_data(segment_data)
                self.segments.append(seg)

    def __repr__(self):
        return "<SequencerTable({0:d} segments)>".format(len(self.segments))


# --------------------------------------------------------------------------------

class Segment(object):
    """Represents a segment in the sequencer table."""

    def __init__(self):
        self.sequence_loop_count = 1

        def _create_property(self, bit):
            """Helper function to define properties for the various bits. """
            return property(
                    lambda self: self.get_control_bit(bit),
                    lambda self, s: self.set_control_bit(bit, s))

        self.control = 0
        Segment.command_segment      = _create_property(self, bit=31)
        Segment.sequence_end_marker  = _create_property(self, bit=30)
        Segment.scenario_end_marker  = _create_property(self, bit=29)
        Segment.sequence_init_marker = _create_property(self, bit=28)
        Segment.marker_enable        = _create_property(self, bit=24)

    def get_control_bit(self, n):
        """Returns the state of the ``n``th control bit."""
        return (self.control >> n) & 1

    def set_control_bit(self, n, state):
        """Sets or clears the ``n``th bit of the control variable."""
        state = int(bool(state))
        self.control = self.control ^ (-state ^ self.control) & (1 << n)

    @property
    def sequence_adv_mode(self):
        bitmask = 0b1111 << 20
        index = (self.control & bitmask) >> 20
        return ADVANCEMENT_MODES[index]

    @sequence_adv_mode.setter
    def sequence_adv_mode(self, value):
        if not value in ADVANCEMENT_MODES:
            raise ValueError("Unknown advancement mode: {0:s}".format(value))
        index = ADVANCEMENT_MODES.index(value)
        self.control = self.control | (index << 20)

    @property
    def segment_adv_mode(self):
        return self._segment_adv_mode

    @segment_adv_mode.setter
    def segment_adv_mode(self, value):
        if not value in ADVANCEMENT_MODES:
            raise ValueError("Unknown advancement mode: {0:s}".format(value))
        self._segment_adv_mode = value

    def get_data(self):
        """Returns the six Int32 values which represent the segment in the
        sequencer table.
        """
        raise NotImplementedError()

    @classmethod
    def from_data(cls, data):
        """Returns the correct segment type for a given set of the six Int32
        variables.
        """
        # Check bit 31 of the first integer if it is a command segment or data
        # segment.
        if (data[0] >> 31) & 1:
            return IdleSegment.from_data(data)
        else:
            return WaveformSegment.from_data(data)


class IdleSegment(Segment):
    """Represents an idle segment in the sequencer table.

    An idle segment is a segment where a constant sample is played for a
    specific amount of time. The time delay is specified in number of samples.
    The minimum and maximum time delay depends on the granularity of the
    waveform generator mode. The sample value is the DAC value.
    """

    def __init__(self, sample, delay):
        super(IdleSegment, self).__init__()
        self.sample = int(sample)
        self.delay  = int(delay)

        self.command_segment = True

    def get_data(self):
        """Returns the six Int32 values which represent the segment in the
        sequencer table.
        """
        command_code = 0
        return [self.control, self.sequence_loop_count, command_code,
                self.sample, self.delay, 0]

    @classmethod
    def from_data(cls, data):
        seg = IdleSegment(0, 0)
        seg.control             = data[0]
        seg.sequence_loop_count = data[1]
        seg.sample              = data[3]
        seg.delay               = data[4]
        return seg

    def __repr__(self):
        return "<IdleSegment(delay={0:f},sample={0:d})>".format(self.delay,
                self.sample)


class WaveformSegment(Segment):
    """Represents a waveform segment in the sequencer table.

    A waveform segment contains the segment ID as well as the segment loop
    count.
    """

    def __init__(self, id, loop_count=1, start=0, end=0xffffffff):
        super(WaveformSegment, self).__init__()
        self.id         = int(id)
        self.loop_count = int(loop_count)
        self.start      = int(start)
        self.end        = int(end)

        self.command_segment = False

    def get_data(self):
        """Returns the six Int32 values which represent the segment in the
        sequencer table.
        """
        return [self.control, self.sequence_loop_count, self.loop_count,
                self.id, self.start, self.end]

    @classmethod
    def from_data(cls, data):
        seg = WaveformSegment(0)
        seg.control             = data[0]
        seg.sequence_loop_count = data[1]
        seg.loop_count          = data[2]
        seg.id                  = data[3]
        seg.start               = data[4]
        seg.end                 = data[5]
        return seg

    def __repr__(self):
        return "<WaveformSegment(id={0:d},repeat={1:d})>".format(self.id,
                self.loop_count)

