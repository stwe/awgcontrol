# -*- coding: utf-8 -*-
#   awgcontrol
#   Copyright (C) 2017 - 2019  Stefan Weichselbaumer
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
This module contains low-level code to interface the Keysight M8190A Arbitrary
Waveform Generator.

The `driver.M8190A` class contains a complete implementation of the VISA
programming interface of the M8190A. It allows to set all important device
parameters, import waveforms and create sequences.

The sequence table contains of entries (segments), which define either a
waveform segment or an idle segment. The `sequence.SequencerTable` class can
be used to manage such a sequene table. The `sequence.WaveformSegment` and
`sequence.IdleSegment` classes represent waveform and idle segments,
respectively.

"""
