# -*- coding: utf-8 -*-
#   awgcontrol
#   Copyright (C) 2017 - 2019  Stefan Weichselbaumer
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""This module implements a high-level API to create sequences and scenarios.

A Sequence consists of several SequenceEntries, which can either be
WaveformEntries or IdleEntries. A scenario consists of several Sequences. The
Scenario and Sequence class provide a function `segments()`, which can be used
to generate the underlying lowlevel Segment entries.
"""

from lowlevel.sequence import WaveformSegment, IdleSegment

class SequenceEntry(object):
    """Represents an entry in the sequence table. 

    This can be either a waveform segment or an idle segment.
    """
    def create_segment(self, **kwargs):
        """Create the segment corresponding to the entry."""
        raise NotImplementedError()

class WaveformEntry(SequenceEntry):
    """A waveform entry."""

    def __init__(self, waveform, **kwargs):
        """Initialize a waveform entry.
        
        waveform : subclass of `waveforms.Waveform`

        Valid keyword arguments
        -----------------------
        marker_enable : Turn on/off sync and sample markers for this segment
                        (default: True)
        loop_count    : Number of iterations for this waveform (default: 1)
        adv_mode      : The segment advance mode (default: auto)
        start         : Sample where playback is started (default: 0)
        end           : Sample where playback is stopped (default: 0xFFFFFFFF)
        """

        self.waveform      = waveform
        self.loop_count    = int(kwargs.get('loop_count', 1))
        self.marker_enable = kwargs.get('marker_enable', True)
        self.adv_mode      = kwargs.get('adv_mode', 'auto')
        self.start         = kwargs.get('start', 0)
        self.end           = kwargs.get('end', 0xffffffff)

    def create_segment(self, **kwargs):
        """Create a waveform segment.

        Parameters
        ----------
        id : The segment ID
        """
        id = kwargs.get('id', None)
        if id is None:
            raise ValueError("WaveformEntry: No segment ID specified")
        ws = WaveformSegment(id, self.loop_count, self.start, self.end)
        ws.segment_adv_mode = self.adv_mode
        ws.marker_enable    = self.marker_enable
        return ws

    def __repr__(self):
        return "<WaveformEntry({},loop_count={:d})>".format(
                self.waveform.__class__, self.loop_count)

class IdleEntry(SequenceEntry):
    """An idle entry."""

    def __init__(self, delay, sample=0):
        """Initialize an idle entry. 

        delay :  The delay in seconds
        sample : The sample which should be played during the pause 
                 (default: 0)
        """
        self.delay = delay
        self.sample = sample

    def create_segment(self, **kwargs):
        """Create the idle segment.

        Parameters
        ----------
        sample_rate : The sample rate of the AWG (sn samples/sec)
        """
        sample_rate = kwargs.get('sample_rate', None)
        if sample_rate is None:
            raise ValueError("IdleEntry: No sample_rate specified")
        delay_samples = self.delay * sample_rate
        return IdleSegment(self.sample, delay_samples)

    def __repr__(self):
        return "<IdleEntry({:.2e}s)>".format(self.delay)


class Sequence(object):
    """Represents a single sequence. 

    A sequence consists of several segments. The first segment is marked with
    the sequence_init_marker flag.
    """
    def __init__(self, loop_count=1, adv_mode='auto'):
        """Initialize a sequence.

        Parameters
        ----------
        loop_count : Number of sequence iterations (default: 1)
        adv_mode   : Sequence advancement mode (default: 'auto')
        """
        self.entries    = []
        self.loop_count = loop_count
        self.adv_mode   = adv_mode

    def segments(self, sample_rate, waveform_ids):
        """Assemble the segment list.

        Here, we need access to:
            The sampling rate.
            The IDs of each waveform (a waveform -> ID dictionary)
        """
        segments = []
        for entry in self.entries:
            if isinstance(entry, WaveformEntry):
                id_ = waveform_ids[entry.waveform]
                segment = entry.create_segment(id=id_)
            elif isinstance(entry, IdleEntry):
                segment = entry.create_segment(sample_rate=sample_rate)
            else:
                raise RuntimeError("Unknown entry: {}".format(entry.__class__))
            segments.append(segment)

        segments[0].sequence_init_marker = True
        segments[0].sequence_adv_mode    = self.adv_mode
        segments[0].sequence_loop_count  = self.loop_count
        segments[-1].sequence_end_marker = True

        return segments

    def append_waveform(self, waveform, **kwargs):
        """Append a waveform entry to the sequence. """
        self.entries.append(WaveformEntry(waveform, **kwargs))

    def append_pause(self, delay, sample=0):
        """Append an idle entry to the sequence."""
        self.entries.append(IdleEntry(delay=delay, sample=sample))

    def __repr__(self):
        return '<Sequence({} entries)>'.format(len(self.entries))

class Scenario(object):
    """Represents a scenario."""

    def __init__(self, start=0, loop_count=1, adv_mode='auto'):
        """Initialize the scenario.

        Parameters
        ----------
        start:      Start index of first sequence (default: 0)
        loop_count: Number of scenario iterations (default: 1).
        adv_mode:   Scenario advancement mode (default: 'auto')
        """
        self.start      = start
        self.loop_count = loop_count
        self.adv_mode   = adv_mode
        self.sequences  = []

    def append(self, sequence):
        """Appends a sequence to the scenario."""
        self.sequences.append(sequence)

    def segments(self, sample_rate, waveform_ids):
        """Assemble the segment list"""
        segments = []
        for sequence in self.sequences:
            # TODO: pass in sample_rate, ID etc.
            segments.extend(sequence.segments(sample_rate, waveform_ids))

        # Mark last segment of the scenario
        segments[-1].scenario_end_marker = True
        return segments

    def __repr__(self):
        return '<Scenario({} sequences)>'.format(len(self.sequences))

