# -*- coding: utf-8 -*-
#   awgcontrol
#   Copyright (C) 2017 - 2019  Stefan Weichselbaumer, Daniela Lutz
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
import copy

import numpy as np

from . import Waveform, Param
#from main import Waveform,Param


class BIRWURST(Waveform):
    def __init__(self,**kwargs):
        """
        BIR4 sequence with WURST pulse as modulation function
        Parameters are:
            - frequency: The IF frequency
            - amplitude: Value between 0 and 1. Percentage value
            - Tp: Duration of pulse
            - n: Specific value for WURST pulses. Normally between 10 and 20
            - omega_max: maximum of possible frequency
            - angle: Rotation angle (in degrees)
        """
        self.param_list = [
            Param('frequency',required=True),
            Param('amplitude',default=1.0),
            Param('Tp',required=True),
            Param('n',default=20),
            Param('omega_max',default=20e6),
            Param('angle',default=np.deg2rad(0))
        ]
        super(BIRWURST, self).__init__(**kwargs)
        self.time = self.Tp
        self.amplitudec = None
        self.phasec = None
    
    def B1(self, ts, Tp):
        beta = np.pi / Tp
        return self.amplitude * (1 - np.abs(np.power(np.sin(beta * ts), self.n)))
    
    def Phase(self, ts, Tp):
        k = 4*np.pi*self.omega_max / Tp
        return 0.5 * k * np.power(ts, 2)
        
    def get_amplitude(self, ts):
        Tp = self.Tp
        
        condlist = [np.logical_and(0 <= ts,      ts <= 0.25*Tp),
                    np.logical_and(0.25*Tp < ts, ts <= 0.50*Tp),
                    np.logical_and(0.50*Tp < ts, ts <= 0.75*Tp),
                    np.logical_and(0.75*Tp < ts, ts <= 1.00*Tp)]
        funclist = [lambda ts: self.B1(ts, self.Tp/2),
                    lambda ts: self.B1(ts - 0.5*Tp, self.Tp/2),
                    lambda ts: self.B1(ts - 0.5*Tp, self.Tp/2),
                    lambda ts: self.B1(ts - Tp, self.Tp/2)]
        amplitude = np.piecewise(ts, condlist, funclist)
        return amplitude

    def get_phase(self, ts):
        Tp = self.Tp
        phi1 = np.pi + np.deg2rad(self.angle / 2)
        phi2 = -(np.pi + np.deg2rad(self.angle / 2))
        
        
        condlist = [np.logical_and(0 <= ts,      ts <= 0.25*Tp),
                    np.logical_and(0.25*Tp < ts, ts <= 0.50*Tp),
                    np.logical_and(0.50*Tp < ts, ts <= 0.75*Tp),
                    np.logical_and(0.75*Tp < ts, ts <= 1.00*Tp)]
        funclist = [lambda ts: self.Phase(ts, self.Tp/2),
                    lambda ts: self.Phase(ts - 0.5*Tp, self.Tp/2) + phi1,
                    lambda ts: self.Phase(ts - 0.5*Tp, self.Tp/2) + phi1,
                    lambda ts: self.Phase(ts - Tp, self.Tp/2) + phi1 + phi2]
#        funclist = [lambda ts: self.Phase(4*ts, self.Tp),
#                    lambda ts: self.Phase(4*ts - 2*Tp, self.Tp) + phi1,
#                    lambda ts: self.Phase(4*ts - 2*Tp, self.Tp) + phi1,
#                    lambda ts: self.Phase(4*ts - 4*Tp, self.Tp) + phi1 + phi2]
        phase = np.piecewise(ts, condlist, funclist)
        return phase + 2*np.pi*self.frequency*ts



"""adiabatic half passage pulses"""
class AHP(Waveform):
    """
    This function deals with BIR4 pulses:
        -frequency: normal frequnency f not 2*pi*f=omega
        -amplitude: The maximal Amplitude. Per default 1
        - Tp: The time length of an impluls
        -Lambda: characteristic constant
        -periods: time, the sequence should be repeatet
        -orientation: Decides if B1_min->B1_max or B1_max _>B1_min
    Initialize the parameters"""

    def __init__(self,**kwargs):
        self.param_list = [
            Param('frequency', required=True),
            Param('amplitude', default=1.0),
            Param('Tp', required=True),
            Param('Lambda', default = 0),
            Param('periods',default = 1),
            Param('beta',default=np.arctan(20)),
            Param('angle',default=np.deg2rad(0)),
            Param('omega_max',default=30e6),
            Param('orientation',default=1)
            
        ]
        super(AHP, self).__init__(**kwargs)
        self.time = self.Tp
        self.phi1 = np.pi-self.angle/2
        self.phi2 = -self.phi1
        self.omega_max = 2*np.pi*self.omega_max  
        self.frequency = 2*np.pi*self.frequency
        self.phasec = None
        self.amplitudec = None
    
    """Calculates the phase for a given time"""
    def calculate_phase(self,ts):
        array = []
        
        for t in ts:
            if self.orientation ==1:
                result = -self.omega_max*self.Tp*np.log(np.cos(self.beta*(1-t/self.Tp)))/(self.beta*np.tan(self.beta))
                result +=t*self.frequency
                array.append(result)
            elif self.orientation == -1:
                result = -self.omega_max*self.Tp*np.log(np.cos(self.beta*(t/self.Tp)))/(self.beta*np.tan(self.beta))
                result +=t*self.frequency
                array.append(result)

#        
        return np.array(array)
        


    """
    Allgemein gilt: Phimax verursacht nur einen Offset, er bringt uns im allgemein gar nichss
    Omegamax duerfte viel zu hoch sein fuer das was wir vorhaben
    Braucht man hier die Kreisfrequenz omega oder f?
    Larmorfrequenz ist viel zu klein im Vergleich  zu self.frequency
    """
    def get_phase(self,ts):
        return self.calculate_phase(ts)
    
    """
    Returns I and Q values by given amplitude and phase
    """
#    def get_iq_from_amplitude(self, ts):
#        A = self.get_amplitude(ts)
#        P= self.get_phase(ts)
#        I = A*np.sin(P)
#        Q = A*np.cos(P)
#        return np.array(I),np.array(Q)
    
#    def get_iq_from_amplitude(self,ts):
#        if self.amplitudec == None and self.phasec == None:
#            I = self.get_amplitude(ts)*np.cos(self.get_phase(ts))
#            Q = self.get_amplitude(ts)*np.sin(self.get_phase(ts))
#            return np.array(I),np.array(Q)
#        else:
#            I = self.amplitudec*np.cos(self.phasec)
#            Q = self.amplitudec*np.sin(self.phasec)
#            return np.array(I),np.array(Q)
    
    """
    Function, that gives back the maximal amplitude
    """
    def Amplitude(self, ts):
        array = []
        for t in ts:
            if self.orientation ==1:
#                t=t/4
                array.append(self.amplitude*np.tanh(self.Lambda*(t/self.Tp)))
            elif self.orientation == -1:
#                t=t/4+self.Tp
                array.append(self.amplitude*np.tanh(self.Lambda*(1-t/self.Tp)))
#            if t >=0 and t< self.Tp/4:
#                array.append(self.amplitude*np.tanh(self.Lambda*(1-4*t/self.Tp)))
#            elif t>=self.Tp/4 and t <self.Tp/2:
#                array.append(self.amplitude*np.tanh(self.Lambda*(4*t/self.Tp-1)))
#            elif t>=self.Tp/2 and t<3*self.Tp/4:
#                array.append(self.amplitude*np.tanh(self.Lambda*(3-4*t/self.Tp)))
#            else:
#                array.append(self.amplitude*np.tanh(self.Lambda*(4*t/self.Tp-3)))
                
        return array
    
    """Function, that returns time and amplitude"""
    def get_amplitude(self,ts):
        return self.Amplitude(ts)



"""Adiabatic full passage pulses"""
class AFP(Waveform):
    """
    This function deals with BIR4 pulses:
        -frequency: normal frequnency f not 2*pi*f=omega
        -amplitude: The maximal Amplitude. Per default 1
        - Tp: The time length of an impluls
        -Lambda: characteristic constant
        -periods: time, the sequence should be repeatet
    Initialize the parameters"""

    def __init__(self,**kwargs):
        self.param_list = [
            Param('frequency', required=True),
            Param('amplitude', default=1.0),
            Param('Tp', required=True),
            Param('Lambda', default = 0),
            Param('periods',default = 1),
            Param('Lambda', default = 0),
            Param('beta',default=np.arctan(20)),
            Param('angle',default=np.deg2rad(0)),
            Param('omega_max',default=30e6)
            
        ]
        super(AFP, self).__init__(**kwargs)
        self.time = self.Tp
        self.phi1 = np.pi-self.angle/2
        self.phi2 = -self.phi1
        self.larmor_frequenz = 1.76e11 * self.amplitude
        self.amplitudec = None
        self.phasec = None
        
    
    """Calculates the phase for a given time"""
    def calculate_phase(self,ts):
        array = []
        
        for t in ts:
            t=t/2+self.Tp/4
            if t >=0 and t< self.Tp/4:
                result = - self.omega_max*self.Tp*np.log(np.cos(self.beta*4*t/self.Tp))/(4*self.beta*np.tan(self.beta))

                result += 2*np.pi*t*self.frequency
                array.append(result)
                
            elif t>=self.Tp/4 and t <self.Tp/2:
                result = -self.omega_max*self.Tp*np.log(np.cos(2*self.beta*(self.Tp-2*t)/self.Tp))/(4*self.beta*np.tan(self.beta))+self.phi1
                
                result += 2*np.pi*t*self.frequency
                array.append(result)
            elif t>=self.Tp/2 and t <=3*self.Tp/4:
                result = -self.omega_max*self.Tp*np.log(np.cos(2*self.beta*(self.Tp-2*t)/self.Tp))/(4*self.beta*np.tan(self.beta))+self.phi1
                
                result += 2*np.pi*t*self.frequency
                array.append(result)
            else:
                result = -self.omega_max*self.Tp*np.log(np.cos(4*self.beta*(self.Tp-t)/self.Tp))/(4*self.beta*np.tan(self.beta))+self.phi1+self.phi2
                
                result += 2*np.pi*t*self.frequency
                array.append(result)
        
        return np.array(array)
        
    """Caculates the frequency for a given time"""
    def frequency(self,ts):
        array = []
        for t in ts:
            t=t/2+self.Tp/4
            if t >=0 and t< self.Tp/4:
                array.append(self.larmor_frequenz - self.omega_max*np.tan(4*self.beta*t/self.Tp)/np.tan(self.beta))
            elif t>=self.Tp/4 and t <self.Tp/2:
                array.append(self.larmor_frequenz - self.omega_max*np.tan(self.beta*(4*t/self.Tp-2))/np.tan(self.beta))
            elif t>=self.Tp/2 and t<3*self.Tp/4:
                array.append(self.larmor_frequenz - self.omega_max*np.tan(self.beta*(4*t/self.Tp-2))/np.tan(self.beta))
            else:
                array.append(self.larmor_frequenz - self.omega_max*np.tan(self.beta*(4*t/self.Tp-4))/np.tan(self.beta))
        return np.array(array)
    
    """Function, that returns time and phase"""
    def get_frequency(self,ts):
        return self.frequency(ts)

    """
    Allgemein gilt: Phimax verursacht nur einen Offset, er bringt uns im allgemein gar nichss
    Omegamax duerfte viel zu hoch sein fuer das was wir vorhaben
    Braucht man hier die Kreisfrequenz omega oder f?
    Larmorfrequenz ist viel zu klein im Vergleich  zu self.frequency
    """
    def get_phase(self,ts):
        return self.calculate_phase(ts)
    
    """
    Returns I and Q values by given amplitude and phase
    """
#    def get_iq_from_amplitude(self, ts):
#        A = self.get_amplitude(ts)
#        P= self.get_phase(ts)
#        I = A*np.cos(P)
#        Q = A*np.sin(P)
#        return np.array(I),np.array(Q)
    
#    def get_iq_from_amplitude(self,ts):
#        if self.amplitudec == None and self.phasec == None:
#            I = self.get_amplitude(ts)*np.cos(self.get_phase(ts))
#            Q = self.get_amplitude(ts)*np.sin(self.get_phase(ts))
#            return I,Q
#        else:
#            I = self.amplitudec*np.cos(self.phasec)
#            Q = self.amplitudec*np.sin(self.phasec)
#            return np.array(I,np.array(Q)
    """
    Function, that gives back the maximal amplitude
    """
    def Amplitude(self, ts):
        array = []
        for t in ts:
            t=t/2+self.Tp/4
            if t >=0 and t< self.Tp/4:
                array.append(self.amplitude*np.tanh(self.Lambda*(1-4*t/self.Tp)))
            elif t>=self.Tp/4 and t <self.Tp/2:
                array.append(self.amplitude*np.tanh(self.Lambda*(4*t/self.Tp-1)))
            elif t>=self.Tp/2 and t<3*self.Tp/4:
                array.append(self.amplitude*np.tanh(self.Lambda*(3-4*t/self.Tp)))
            else:
                array.append(self.amplitude*np.tanh(self.Lambda*(4*t/self.Tp-3)))
                
        return np.array(array)
    
    """Function, that returns time and amplitude"""
    def get_amplitude(self,ts):
        return self.Amplitude(ts)


"""Wurst 20 pulse"""
class WURST_AHP(Waveform):
    def __init__(self,**kwargs):
        """
        Parameters are:
            - frequency: The IF frequency
            - amplitude: Value between 0 and 1. Percentage value
            - Tp: Duration of pulse
            - n: Specific value for WURST-20 pulses. Normally between 10 and 20
            - beta: internal value????
            - omega_max: maximum of possible frequency
        """
        self.param_list = [
            Param('frequency',required=True),
            Param('amplitude',default=1.0),
            Param('Tp',required=True),
            Param('n',default=20),
            Param('omega_max',default=20e6),
        ]
        super(WURST_AHP, self).__init__(**kwargs)
        self.time = self.Tp
        self.amplitudec = None
        self.phasec = None
    
    def B1(self, ts, Tp):
        beta = np.pi / Tp
        return self.amplitude * (1 - np.abs(np.power(np.sin(beta * (ts - Tp/2)), self.n)))
    
    def Phase(self, ts, Tp):
        k = -2*np.pi*self.omega_max / Tp
        return 0.5 * k * np.power(ts - Tp/2, 2)
        
    def get_amplitude(self, ts):
        Tp = self.Tp * 2
        return self.B1(ts, Tp)

    def get_phase(self, ts):
        Tp = self.Tp * 2
        return self.Phase(ts, Tp) + 2*np.pi*self.frequency*ts
 
    

 

"""Wurst 20 pulse"""
class WURST(Waveform):
    def __init__(self,**kwargs):
        """
        Parameters are:
            - frequency: The IF frequency
            - amplitude: Value between 0 and 1. Percentage value
            - Tp: Duration of pulse
            - n: Specific value for WURST-20 pulses. Normally between 10 and 20
            - beta: internal value????
            - omega_max: maximum of possible frequency
        """
        self.param_list = [
            Param('frequency',required=True),
            Param('amplitude',default=1.0),
            Param('Tp',required=True),
            Param('n',default=20),
            Param('omega_max',default=20e6)
        ]
        super(WURST, self).__init__(**kwargs)
        self.time = self.Tp
        self.beta = np.pi/(self.Tp)
        self.phi_0 = 0
        self.k = 2*self.omega_max/self.Tp
        self.amplitudec = None
        self.phasec = None
    
#    """
#    Returns I and Q values by given amplitude and phase
#    """
#    def get_iq_from_amplitude(self, ts):
#        A = self.amplitude*(1-abs(np.sin(self.beta*(ts-self.Tp/2)))**self.n)
#        P= self.phi_0 +(1/2)*2*np.pi*self.k*(ts-self.Tp/2)**2+self.frequency*2*np.pi*ts
#        I = A*np.cos(P)
#        Q = A*np.sin(P)
#        return I, Q
    
#    def get_iq_from_amplitude(self,ts):
#        if self.amplitudec == None and self.phasec == None:
#            I = self.get_amplitude(ts)*np.cos(self.get_phase(ts))
#            Q = self.get_amplitude(ts)*np.sin(self.get_phase(ts))
#            return I,Q
#        else:
#            I = self.amplitudec*np.cos(self.phasec)
#            Q = self.amplitudec*np.sin(self.phasec)
#            return I,Q
            
    """Get amplitude"""
    def get_amplitude(self,ts):
        return self.amplitude*(1-abs(np.sin(self.beta*(ts-self.Tp/2)))**self.n)
       
    
    
    """A function that should calculate an optimal value for beta. Is replaced by the condition beta = pi/Tp
    Therefore not needed anymore"""
    def testBeta(self):
        try:
            time = np.linspace(0, self.Tp, 200)
            value = True
            for t in time:
                if self.beta*t>np.pi/2:
                    print("No")
                    self.beta = np.pi/(4*t)
                    value = False
                    break
                elif self.beta*t<-np.pi/2:
                    print("no")
                    self.beta = -np.pi/(4*t)
                    value =False
                    break
                else:
                    print("passt")
            
            if value == False:
                self.testBeta()
        except:
            print("MEH")
        
        
    """Function that gives back the phase,
    Does IF need a time shift"""
    def get_phase(self,ts):
        return self.phi_0 +(1/2)*self.k*(ts-self.Tp/2)**2+self.frequency*2*np.pi*ts
        
    """Formula for frequency"""
    def get_frequency(self,ts):
        return self.k*(ts)
        
    

        
    
class BIR1(Waveform):
    """
    This function deals with BIR4 pulses and a AHP as modulation function:
        -frequency: IF frequency
        -amplitude: The maximal Amplitude. Per default 1
        -Tp: The length of the complete pulse
        -kappa, zeta: pulse parameters from optimization
        -omega_max: Maximum detuning (in frequency(Hz))
        -angle: rotation angle of spin
        """
    def __init__(self, **kwargs):
        self.param_list = [
            Param('frequency',required=True),
            Param('amplitude',default=1.0),
            Param('Tp',required=True),
            Param('beta', required=True),
            Param('Lambda', required=True),
            Param('omega_max',required=True),
            Param('angle',default=np.deg2rad(0))
        ]
        super(BIR1, self).__init__(**kwargs)
        self.time = self.Tp
        self.frequency = 2*np.pi*self.frequency
        self.omega_max = 2*np.pi*self.omega_max
        self.angle = np.deg2rad(self.angle)
        self.amplitudec = None
        self.phasec = None
        
    def B1(self,t,minus):
        return minus*self.amplitude*np.tanh(self.Lambda*2*(t))
        
    
    
    def get_amplitude(self, ts):
        Tp = self.Tp
        
        condlist = [np.logical_and(0 <= ts, ts <= 0.5*Tp),
                    np.logical_and(0.5*Tp < ts, ts <= 1.00*Tp)]
        funclist = [lambda ts: self.B1(ts/Tp,1),
                    lambda ts: self.B1(1-ts/Tp,-1)]
        amplitude = np.piecewise(ts, condlist, funclist)
        return amplitude

    def Phase(self,t,minus):
        return (-self.omega_max*self.Tp*np.log(np.cos(self.beta*(1-2*t/self.Tp)))/(2*self.beta*np.tan(self.beta)))*minus+self.frequency*t

    def get_phase(self,ts):
        phase = np.pi + self.angle
        Tp = self.Tp
        condlist = [np.logical_and(0 <= ts, ts <= 0.5*Tp),
                    np.logical_and(0.5*Tp < ts, ts <= 1.00*Tp)]
        funclist = [lambda ts: self.Phase(ts,1),
                    lambda ts: self.Phase(ts,-1)+phase]
        phase = np.piecewise(ts, condlist, funclist)
        return phase

class BIR4(Waveform):
    """
    This function deals with BIR4 pulses and a AHP as modulation function:
        -frequency: IF frequency
        -amplitude: The maximal Amplitude. Per default 1
        -Tp: The length of the complete pulse
        -kappa, zeta: pulse parameters from optimization
        -omega_max: Maximum detuning (in frequency(Hz))
        -angle: rotation angle of spin
        """
    def __init__(self, **kwargs):
        self.param_list = [
            Param('frequency',required=True),
            Param('amplitude',default=1.0),
            Param('Tp',required=True),
            Param('kappa', required=True),
            Param('zeta', required=True),
            Param('omega_max',required=True),
            Param('angle',default=np.deg2rad(0))
        ]
        super(BIR4, self).__init__(**kwargs)
        self.time = self.Tp
        self.frequency = 2*np.pi*self.frequency
        self.omega_max = 2*np.pi*self.omega_max
        self.angle = np.deg2rad(self.angle)
        self.amplitudec = None
        self.phasec = None
        
    def B1(self, ts, Tp):
        return self.amplitude * np.tanh(self.zeta * ts)
    
    def Phase(self, ts, t,Tp):
        kappa = self.kappa
        return -self.omega_max*Tp*np.log(np.cos(kappa*ts))/(4*kappa*np.tan(kappa))
#        return self.omega_max * np.tan(self.kappa * (1 - ts / Tp)) / np.tan(self.kappa)
        
#        phi_max = self.omega_max*(Tp/4) * np.log(np.cos(kappa*0))/(kappa * np.tan(kappa)) 
#        val = phi_max-self.omega_max * (Tp/4) *np.log(np.cos(kappa * (ts)))/(kappa * np.tan(kappa))
#        return val
    
    def get_amplitude(self, ts):
        Tp = self.Tp
        
        condlist = [np.logical_and(0 <= ts,      ts <= 0.25*Tp),
                    np.logical_and(0.25*Tp < ts, ts <= 0.50*Tp),
                    np.logical_and(0.50*Tp < ts, ts <= 0.75*Tp),
                    np.logical_and(0.75*Tp < ts, ts <= 1.00*Tp)]
        funclist = [lambda ts: self.B1(1-4*ts/Tp, Tp),
                    lambda ts: self.B1(4*ts/Tp-1, Tp),
                    lambda ts: self.B1(3-4*ts/Tp, Tp),
                    lambda ts: self.B1(4*ts/Tp-3, Tp)]
        amplitude = np.piecewise(ts, condlist, funclist)
        return amplitude

    def get_phase(self, ts):
        Tp = self.Tp
        phi1 = np.pi + self.angle / 2
#        phi2 = -(np.pi + self.angle / 2)
        phi2 = -phi1
        
        condlist = [np.logical_and(0 <= ts,      ts <= 0.25*Tp),
                    np.logical_and(0.25*Tp < ts, ts <= 0.50*Tp),
                    np.logical_and(0.50*Tp < ts, ts <= 0.75*Tp),
                    np.logical_and(0.75*Tp < ts, ts <= 1.00*Tp)]
        funclist = [lambda ts: self.Phase(4*ts/Tp,ts, self.Tp),
                    lambda ts: self.Phase(2-4*ts/Tp,ts, self.Tp) + phi1,
                    lambda ts: self.Phase(2-4*ts/Tp,ts, self.Tp) + phi1,
                    lambda ts: self.Phase(4-4*ts/Tp,ts, self.Tp) + phi1 + phi2]
        phase = np.piecewise(ts, condlist, funclist)
        return phase + self.frequency*ts
