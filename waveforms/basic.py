# -*- coding: utf-8 -*-
#   awgcontrol
#   Copyright (C) 2017 - 2019  Stefan Weichselbaumer
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
import numpy as np
from scipy.signal import square, sawtooth, gausspulse

from . import Waveform, Param


class Constant(Waveform):
    """This waveform generates a constant voltage on the I channel. The Q
    channel is set to zero.

    - pulse_length: The length of the  constant voltage pulse
    - sample: The sample which is generated. Has to be within -1 and 1.
    - component: The component where the pulse is created (either I or Q).
    - delay: Delay time before the pulse starts.

    """
    def __init__(self, **kwargs):
        self.param_list = [
            Param('pulse_length', required=True),
            Param('sample',      required=True),
            Param('delay',       default=0),
            Param('component',   default='I'),
        ]
        super(Constant, self).__init__(**kwargs)
        self.time = self.pulse_length + self.delay

    def get_iq_from_amplitude(self, ts):
        """Override the default IQ calculation. Here, we set I to the constant
        voltage pulse and Q to zero.
        """
        samples = self.sample * np.ones_like(ts)
        samples[ts < self.delay] = 0
        samples[-1] = 0

        if self.component.upper() == 'I':
            I = samples
            Q = np.zeros_like(ts)
        elif self.component.upper() == 'Q':
            I  = np.zeros_like(ts)
            Q = samples
        else:
            raise ValueError("Unknown component: {0}".format(self.component))

        return I, Q


class EnvelopeModulationWaveform(Waveform):
    """This waveform multiplies a carrier signal (typically at the
    intermediate frequency) with an envelope modulation function.

    The EnvelopeModulationWaveform takes arguments defining the correct phase
    of the carrier signal and can shift the phase between I and Q.

    - frequency:     The frequency of the carrier signal
    - carrier_phase: The phase of the carrier signal
    - phase:         The phase of I and Q
    """
    def __init__(self, **kwargs):
        self.param_list.extend([
            Param('carrier_phase', default=0),
            Param('phase', default=0),
        ])
        super(EnvelopeModulationWaveform, self).__init__(**kwargs)

    def get_envelope(self, ts):
        raise NotImplementedError("The EnvelopeModulationWaveform has to be "
                                  "subclassed to be used")

    def get_iq_from_amplitude(self, ts):
        # Generate carrier signal
        carrier_phase_factor = np.exp(1j*np.deg2rad(self.carrier_phase))
        carrier = carrier_phase_factor * np.exp(2j*np.pi*self.frequency*ts)

        # Generate I and Q from carrier signal 
        phase_factor = np.exp(1j*np.deg2rad(self.phase))
        envelope = self.get_envelope(ts)
        I = envelope * np.real(carrier*phase_factor)
        Q = envelope * np.imag(carrier*phase_factor)

        return I, Q


class Rectangle(Waveform):
    """Simple rectangular waveform. It is ``2*pi`` periodic.

    - frequency: The frequency of the rectangular waveform.
    - duty: Duty cycle of the waveform. Defaults to 0.5. 
    - amplitude: The amplitude parameter scales the values of the waveform.
    - phase: The phase can be used to shift the value at t=0.
    - offset: The offset shifts the values of the waveform by a constant
      amount.
    - periods: Defines how many periods of the waveform are generated.
    """
    def __init__(self, **kwargs):
        self.param_list = [
            Param('frequency', required=True, unit='Hz'),
            Param('duty',      default=1),
            Param('amplitude', default=0.5),
            Param('phase',     default=0, unit='degree'),
            Param('offset',    default=0.5),
            Param('periods',   default=1),
        ]
        super(Rectangle, self).__init__(**kwargs)
        self.time = self.periods * 1/self.frequency

    def get_amplitude(self, ts):
        ys = (self.offset + 
              self.amplitude * square(2*np.pi*self.frequency*ts,
                                      duty=self.duty))
        return ys

    def get_phase(self, ts):
        phase = np.deg2rad(self.phase)
        return np.zeros_like(ts) + phase


class Sine(EnvelopeModulationWaveform):
    """Simple sinusoidal waveform.

    - frequency: The frequency of  the sine.
    - phase: The phase can be used to generate sine or cosine.
    - amplitude: Scales the amplitude of the waveform
    - periods: Defines how many periods are generated.
    """

    def __init__(self, **kwargs):
        self.param_list = [
            Param('amplitude', default=1.0),
            Param('periods',   default=1),
        ]
        super(Sine, self).__init__(**kwargs)
        self.time = self.periods * 1/self.frequency

    def get_envelope(self, ts):
        return self.amplitude * np.ones(ts.shape)


class Gaussian(EnvelopeModulationWaveform):
    """Sinusoidal waveform with a gaussian envelope.

    - amplitude: Amplitude of the Gaussian (default: 1)
    - Tp:        FWHM of Gaussian envelope
    """

    def __init__(self, **kwargs):
        self.param_list = [
            Param('amplitude', default=1.0),
            Param('Tp', required=True),
        ]
        super(Gaussian, self).__init__(**kwargs)
        # The real pulse time depends on the resolution of the AWG. At a
        # certain point, the difference of the voltage of sample `x` and
        # sample `x+1` is smaller than the resolution of the AWG (given by the
        # full-scale output of 0.35 V and 12 bit resolution). 
        # If you calculate this point with some security margin we end up
        # with:
        self.time = self.Tp * np.sqrt(14)

    def get_envelope(self, ts):
        sigma = self.Tp / np.sqrt(8*np.log(2))
        t0 = self.time / 2
        envelope = self.amplitude*np.exp(-(ts-t0)**2/(2*sigma**2))
        return envelope
