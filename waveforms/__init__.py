# -*- coding: utf-8 -*-
#   awgcontrol
#   Copyright (C) 2017 - 2019  Stefan Weichselbaumer
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
import collections
import operator
import numpy as np

import logging

class Sample(object):
    """Represents a single sample. It contains the time, the sample value and
    flags for sync and sample markers.
    """
    __slots__ = ['time', 'value', 'sync', 'sample']

    def __init__(self, time, value, sync=False, sample=False):
        self.time = time
        self.value = value
        self.sync = sync
        self.sample = sample

    def __repr__(self):
        return '<Sample(value=%f, sync=%s, sample=%s)>' % (
                    self.value, self.sync, self.sample)


class Param(object):
    """A parameter stores parametric data of waveform functions. It has the
    following attributes:

        - name: The name of the parameter
        - value: The current value of the parameter
        - default: The initializiation value of the parameter
        - required: Required parameters have to be set before waveforms can be
          generated (default: False).
        - hide: Hidden parameters are not shown in the GUI (default: False).
        - unit: The physical unit of the parameter (default: None).
        - docstr: A docstring which describes the parameter.
    """

    def __init__(self, name, default=None, required=False, hide=False,
                 unit=None, docstr=None):
        self.name = name
        self.required = required
        self.hide = hide
        self.docstr = docstr or ''
        self.unit = unit
        self.default = default
        self.value = self.default

    def __repr__(self):
        return "<Param({0:s}={1:s},hide={2:b})>".format(self.name,
                str(self.value), self.hide)

    def __eq__(self, other):
        return (isinstance(other, self.__class__) and 
                self.__dict__ == other.__dict__)

    def __hash__(self):
        return hash((self.name, self.value))
    


class Waveform(object):
    """Represents waveforms in the physical sense, i.e. time-continuous
    functions which are defined for every point in time. A waveform contains
    some basic parameters:

    - time: The length of time during which the waveform is sampled.
    - sync_marker: A list of time dates where sync markers are generated.
    - sample_marker A list of time dates where sample markers are generated.

    Subclasses can define additional parameters by setting the ``param_list``
    class property to a list of Param objects.

    The get_samples() function can be used to calculate a list of samples for
    a given sample_rate. This function obeys the granularity of the AWG. This
    function calls the calculate() function, which has to be implemented by
    each subclass. Note that calculate() should return values normalized to
    the interval [-1, 1].
    """
    def __init__(self, **kwargs):
        """Initialize a waveform. Additional key-value arguments are parsed as
        parameters.
        """
        self.param_list.extend([
            Param('time', required=True, hide=True, unit='s'),
            Param('sync_marker',   default=[], hide=True),
            Param('sample_marker', default=[], hide=True)
        ])
        self._params        = {p.name: p for p in self.param_list}
        
        self.log = logging.getLogger('Waveform')

        for name, param in kwargs.items():
            setattr(self, name, param)

    def add_param(self, param):
        self._params[param.name] = param

    def set(self, name, value):
        self._params[name].value = value

    def get(self, name):
        return self._params[name].value

    def __getattribute__(self, name):
        try:
            params = object.__getattribute__(self, '_params')
            if name in params:
                return params[name].value
        except AttributeError:
            pass
        return object.__getattribute__(self, name)

    def __setattr__(self, name, value):
        if hasattr(self, '_params') and name in self._params:
            self._params[name].value = value
        else:
            object.__setattr__(self, name, value)

    def check_required_params(self):
        """Checks if all required parameters were set."""
        for name, param in self._params.items():
            if param.required and param.value is None:
                raise ValueError("Required parameter {0:s} has not been "
                                 "set".format(name))

    def get_amplitude(self, ts):
        """Calculate the amplitude of the function."""
        raise NotImplementedError()

    def get_phase(self, ts):
        """Calculate the phase of the function."""
        raise NotImplementedError()

    def get_iq_from_amplitude(self, ts):
        """Calculate the I and Q component from the sample waveform"""
        I = self.get_amplitude(ts) * np.sin(self.get_phase(ts))
        Q = self.get_amplitude(ts) * np.cos(self.get_phase(ts))
        return I, Q

    def adjust_time(self, sample_rate, granularity):
        """Adjusts the time of the waveform so that the number of samples is a
        multiple of granularity.
        """
        
        self.log.debug('sample_rate = {:f}, granularity = {:d}, time = {:f}'.format(
                  sample_rate, granularity, self.time))
                  
        no_samples = int(self.time * sample_rate)
        tmax = self.time
        

        # If number of samples is not a multiple of granularity, we round up
        # to the next multiple (i.e. add more samples).
        if no_samples % granularity != 0:
            no_samples = np.ceil(float(no_samples) / granularity)
            no_samples = int(no_samples * granularity)
            tmax = no_samples / sample_rate

        self.time = tmax

    def get_iq(self, sample_rate, granularity):
        """Calculate the samples for the in-phase and quadrature component of
        the waveform for a given sample rate and time interval.
        """
        self.check_required_params()

        if self.time == 0:
               raise ValueError("Incorrect time_interval: {0:f}".format(
                                self.time))

        # Check if time interval matches granularity and round tmax to the
        # next possible value.
        self.adjust_time(sample_rate, granularity)

        # Generate list of time points and corresponding values
        no_samples = np.round(self.time * sample_rate)
        ts = np.linspace(0, self.time, no_samples, endpoint=False)

        I, Q = self.get_iq_from_amplitude(ts)
        # Set last sample to zero, so that the output is zero after the
        # waveform
        #I[-1] = Q[-1] = 0

        I = [Sample(t, v) for t, v in zip(ts, I)]
        Q = [Sample(t, v) for t, v in zip(ts, Q)]

        # Next, calculate the sample positions of the sync and sample markers
        sync_positions = [int(t/self.time*(no_samples-1))
                          for t in self.sync_marker]
        sample_positions = [int(t/self.time*(no_samples-1))
                            for t in self.sample_marker]
        for s in sync_positions:
            I[s].sync = True
            Q[s].sync = True
        for s in sample_positions:
            I[s].sample = True
            Q[s].sample = True

        return I, Q

    def __eq__(self, other):
        #return (isinstance(other, self.__class__) and 
        #        self.__class__ == other.__class__ and
        #        self._params == other._params)
        return id(self) == id(other)

    def __hash__(self):
        hashables = []
        for p in self._params.values():
            if isinstance(p.value, collections.Hashable):
                hashables.append((p.name, p.value))
        hashables = tuple(hashables)
        return hash((self.__class__, hashables))

