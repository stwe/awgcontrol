# awgcontrol

The awgcontrol server program provides an interface to program a Keysight
M8190A arbitrary waveform generator. The server program accepts commands via a
TCP/IP connection and can be used to generate pulse sequences for a electron
spin resonance spectrometer.

The command protocol is described in the file `server.py`. `controller.py`
contains a high-level class to access the arbitrary waveform generator. Common
pulse sequences (Hahn echo, inversion recovery) are defined in
`experiments.py`. 

The low-level SCPI protocoll implementation is in the `lowlevel` package. The
`waveform` package contains the definition of basic as well as more advanced
pulse waveforms.

```
usage: awgcontrol [-h] [-v VISA_ADDRESS] [-a ADDRESS] [-p PORT] [-l LOGLEVEL]
                  [-f LOGFILE] [--version]

optional arguments:
  -h, --help            show this help message and exit
  -v VISA_ADDRESS, --visa-address VISA_ADDRESS
                        VISA address of the M8190A AWG (default:
                        TCPIP0::localhost::inst0::INSTR)
  -a ADDRESS, --address ADDRESS
                        address where the server binds to (default: 127.0.0.1)
  -p PORT, --port PORT  port to listen for connections (default: 10000)
  -l LOGLEVEL, --loglevel LOGLEVEL
                        set the loglevel (DEBUG, INFO, WARNING, ERROR)
                        (default: INFO)
  -f LOGFILE, --logfile LOGFILE
                        log to file (default: off) (default: None)
  --version             show program's version number and exit
```
