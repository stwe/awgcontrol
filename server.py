# -*- coding: utf-8 -*-
#   awgcontrol
#   Copyright (C) 2017 - 2019  Stefan Weichselbaumer
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import socket
import socketserver
import traceback

from controller import AWGController
from experiments import hahn_echo, inv_recovery, sat_recovery
import waveforms

"""Implements a TCP server which takes requests and performs AWG
initialization and configuration.

The requests must have the following form::

    <DEV>:<COMMAND>:<PARAMETERS>

For <DEV> currently only `AWG` is supported. The <PARAMETERS> list is a
comma-separated list of "key=value" pairs, e.g. `level=0.5,mode=triggered`.

The supported commands are:

- TRIGGER: Configure the trigger settings. Valid params are:

  * level     : Trigger input threshold level (in V)
  * impedance : Trigger impedance (`high`: 1 kOhm, `low`: 50 Ohm)
  * slope     : Trigger on `rising`, `falling` or `either` edges.
  * source    : Trigger source (`internal` or `external`)
  * frequency : Internal trigger frequency
  * mode      : Trigger mode (continuous, triggered, gated)

- EVENT: Configure the event system. 

  * level     : Event input threshold level (in V)
  * impedance : Event input impedance (`high`: 1kOhm, `low`: 50 Ohm)
  * slope     : Event on `rising`, `falling` or `either`
  
 - OUTPUT: Configure the output

  * amplitude : The amplitude of the generated signal in Volt
  * route     : The output route (either 'dac', 'dc' or 'ac')

- MARKER: Configure the marker system. 

  * sample_high : High level of the sample marker
  * sample_low  : Low level of the sample marker
  * sync_high   : High level of the sync marker
  * sync_low    : Low level of the sync marker

- SAMPLERATE: Set the sample rate.

  * sample_rate : The desired sample rate in Hz.
  
- SINE: Generate a sine waveform and start generation 

  * frequency :  Frequency of the sine waveform
  * phase     : Phase of the sine wave (degree)
  * amplitude : Amplitude of the sine wave (0..1)
              
- HAHN: Upload a Hahn echo sequence and start signal generation.

  * frequency  : The frequency of the carrier/IF signal
  * Tpihalf    : Length of the pi half pulse
  * Tpi        : Length of the pi pulse
  * Tau        : Time between pulses
  * Tacq       : Length of acquisition window
  * Apihalf    : Amplitude of the pi half pulse (default: 1)
  * Api        : Amplitude of the pi pulse (default: 1)
  * phaseshift : Phase shift between pi half and pi pulse (default: 90 deg)
  * shape      : Pulse shape (gaussian or rect, default: gaussian)
  * averages   : Number of averages 
  * srt        : Shot-repetition-rate, i.e. time between subsequent averages
  * record_pulses : Set to True to also record the generated pulses
  
- INVREC: Upload a inversion recovery sequence and start signal generation.

  * frequency  : The frequency of the carrier/IF signal
  * Tpihalf    : Length of the pi half pulse
  * Tpi        : Length of the pi pulse
  * T          : Time between initial pi pulse and Hahn echo sequence
  * Tau        : Time between pulses
  * Tacq       : Length of acquisition window
  * Apihalf    : Amplitude of the pi half pulse (default: 1)
  * Api        : Amplitude of the pi pulse (default: 1)
  * phaseshift : Phase shift between pi half and pi pulse (default: 90 deg)
  * shape      : Pulse shape (gaussian or rect, default: gaussian)
  * averages   : Number of averages 
  * srt        : Shot-repetition-rate, i.e. time between subsequent averages
  * record_pulses : Set to True to also record the generated pulses
  

If a command is executed properly, the server responds with `OK\r\n`.
Otherwise, a error message is sent back.
"""

READ_BUFSIZE = 4096

class BadRequestError(Exception):
    pass

class AWGServerRequestHandler(socketserver.BaseRequestHandler):
    def setup(self):
        self.continue_running = True
        self._route = 'dac'
        self._amplitude = 0.7
        self.awg = self.server.awg
        self.log = logging.getLogger('RequestHandler')

        self.functions = {
            'AWG': {
                'TRIGGER': {
                    'func': self.awg.configure_trigger,
                    'valid_params': ['level', 'impedance', 'slope', 'source',
                                     'frequency', 'mode']
                },
                'EVENT': {
                    'func': self.awg.configure_event,
                    'valid_params': ['level', 'impedance', 'slope'],
                },
                'MARKER': {
                    'func': self.awg.configure_marker,
                    'valid_params': ['sample_low', 'sample_high', 'sync_low',
                                     'sync_high']
                },
                'SAMPLERATE': {
                    'func': self._set_sample_rate,
                    'valid_params': ['sample_rate'],
                    'required_params': ['sample_rate']
                },
                'OUTPUT': {
                    'func': self._set_output,
                    'valid_params': ['route', 'amplitude'],
                    'required_params': ['route', 'amplitude'],
                },
                'SINE': {
                    'func': self._create_sine,
                    'valid_params': ['frequency', 'phase', 'amplitude'],
                    'required_params': ['frequency']
                },
                'HAHN': {
                    'func': self._create_hahn_echo,
                    'valid_params': ['frequency', 'Tpihalf', 'Tpi', 'Tau',
                                     'Tacq', 'Apihalf', 'Api', 'phaseshift',
                                     'shape', 'averages', 'srt', 'record_pulses','fres','quality'],
                    'required_params': ['frequency', 'Tpihalf', 'Tpi', 'Tacq']
                },
                'INVREC': {
                    'func': self._create_inv_recovery,
                    'valid_params': ['frequency', 'Tpihalf',
                                     'Tpi', 'T', 'Tau', 'Tacq', 'Apihalf',
                                     'Api', 'phaseshift', 'shape', 'averages',
                                     'srt', 'record_pulses'],
                    'required_params': ['frequency', 'Tpihalf', 'Tpi', 'T',
                                        'Tau', 'Tacq']
                },
                'SATREC': {
                    'func': self._create_sat_recovery,
                    'valid_params': ['frequency', 'Tsat', 'T', 'Tpihalf',
                        'Tau', 'Tpi', 'Tacq', 'Asat', 'Apihalf', 'Api',
                        'phaseshift', 'shape', 'averages'],
                    'required_params': ['frequency', 'Tsat', 'T', 'Tpihalf',
                        'Tau', 'Tpi', 'Tacq'],
                },
                'BEGIN': {
                    'func': self._begin,
                    'valid_params': ['a']
                }
            }
        }

    def write(self, msg):
        try:
            response = bytearray(msg + '\r\n', encoding='utf-8')
            self.log.debug("Server response: %s", response)
            self.request.sendall(response)
        except socket.error as err:
            self.log.error("write() failed: {}".format(err))
            self.continue_running = False

    def _begin(self, **kwargs):
        self.log.debug("Triggering awg.")
        self.awg.driver.trigger.begin(channel=1)
        self.awg.driver.trigger.begin(channel=2)

    def _set_sample_rate(self, sample_rate):
        self.awg.sample_rate = sample_rate

    def _set_output(self, route, amplitude):
        self.awg.set_output(route, amplitude)

    def _create_sine(self, **kwargs):
        frequency = kwargs.get('frequency')
        phase     = kwargs.get('phase', 0)
        amplitude = kwargs.get('amplitude', 1.0)
        
        """Bestimmt nicht korrekt"""
        #self.awg.sample_rate = 64*frequency
        self.awg.driver.abort()
        wf = waveforms.basic.Sine(frequency=frequency, phase=phase,
                                  periods=4, amplitude=amplitude)
        self.awg.run_arbitrary(wf)
        self.awg.start_generation()
        

    def _create_hahn_echo(self, **kwargs):
        """Wrapper dispatch function which creates a Hahn echo sequence,
        uploads it to the AWG and starts signal generation.
        """
        self.awg.driver.abort()
        seq = hahn_echo(sample_rate=self.awg.sample_rate, **kwargs)
        self.awg.run_sequence(seq)
        print("self._amplitude", self._amplitude)
        self.awg.start_generation()

    def _create_hahn_echo_ad(self, **kwargs):
        """Wrapper dispatch function which creates a Hahn echo sequence,
        uploads it to the AWG and starts signal generation.
        """
        self.awg.driver.abort()
        seq = hahn_echo_ad(sample_rate=self.awg.sample_rate, **kwargs)
        log = logging.getLogger('HahnEchoAd')
        log.debug("Run sequence")
        self.awg.run_sequence(seq)
        log.debug("Start generation")
        print("self._amplitude", self._amplitude)
        self.awg.start_generation()

    def _create_inv_recovery(self, **kwargs):
        """Wrapper dispatch function which creates a inversion recovery
        sequence, uploads it to the AWG and st arts signal generation.
        """
        self.awg.driver.abort()
        seq = inv_recovery(sample_rate=self.awg.sample_rate, **kwargs)
        self.awg.run_sequence(seq)
        self.awg.start_generation()

    def _create_sat_recovery(self, **kwargs):
        self.awg.driver.abort()
        seq = sat_recovery(sample_rate=self.awg.sample_rate, **kwargs)
        self.awg.run_sequence(seq)
        self.awg.start_generation()

    def dispatch_message(self, message):
        """Parses `message` and calls the corresponding function of the
        controller.
        """
        try:
            name, command, params = message.split(':', 2)
            fnct = self.functions[name][command]
            
        except ValueError:
            raise BadRequestError("Invalid request: {}".format(message))
        except KeyError as e:
            raise BadRequestError("Unknown controller or function specified: "
                                  "{}".format(e.args[0]))

        # Parse parameter list
        params = params.split(',')
        param_dict = {}
        for param in params:
            try:
                key, value = param.split('=', 1)
            except ValueError:
                raise BadRequestError("Invalid parameter specification: "
                                      "{}".format(param))
            # Try to convert value to float
            try:
                value = float(value)
            except ValueError:
                value = str(value)
            param_dict[key] = value

        # Check if all required parameters are set
        if 'required_params' in fnct:
            required_set = False
            for key in fnct['required_params']:
                if not key in param_dict:
                    raise BadRequestError("Required parameter not set: "
                                          "{}".format(key))
        # Check if all parameters are valid
        for key in param_dict:
            if not key in fnct['valid_params']:
                raise BadRequestError("Unknown parameter: {}".format(key))

        # Execute function with parameters as keyword arguments
        try:
            fnct['func'](**param_dict)
        except Exception as e:
            raise BadRequestError("Function execution failed: "
                    "Type={}, Trace:\n{}".format(str(e.__class__.__name__),
                        traceback.format_exc()))

        #error_list = self.awg.driver.system.error_list()
        #self.log.info('\n'.join(str(e) for e in error_list))

        self.write('OK')

    def handle(self):
        while self.continue_running:
            try:
                data = self.request.recv(READ_BUFSIZE)
                self.log.debug("Message received")
                if len(data) == 0:
                    break
                data = data.strip()
            except socket.error as err:
                self.log.error("read() failed: {:s}".format(err))
                break
            self.log.debug('Received request: {:s}'.format(data.decode('utf-8')))

            data = data.decode('utf-8')
            try:
                self.dispatch_message(data)
                self.log.debug('Request dispatched')
            except BadRequestError as e:
                self.write('Error: {:s}'.format(e.args[0]))

class AWGControlServer(socketserver.TCPServer):
    """Implements the awgcontrol main server."""

    def __init__(self, address, handler, awg):
        self.awg      = awg
        socketserver.TCPServer.__init__(self, address, handler)

    def run(self):
        try:
            self.serve_forever()
        except KeyboardInterrupt:
            pass
        finally:
            self.server_close()
        

def start_server(awg, host, port):
    """Starts the awgcontrol main server.  """
    logger = logging.getLogger('server')
    logger.info('Start listening on {:s}:{:d}'.format(host, port))
    socketserver.TCPServer.allow_reuse_address = True
    server = AWGControlServer((host, port), AWGServerRequestHandler, awg)
    server.run()

