# -*- coding: utf-8 -*-
#   awgcontrol
#   Copyright (C) 2017 - 2019  Stefan Weichselbaumer
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
import argparse
import logging

from controller import AWGController
from server import start_server

DEFAULT_LOGFORMAT = "%(asctime)-15s [%(levelname)-8s] %(name)-20s %(message)s"

def setup_logging(loglevel=logging.INFO, logfile=None):
    """Set up logging for the awgcontrol program."""
    
    logging.basicConfig(level=loglevel, format=DEFAULT_LOGFORMAT,
                        datefmt='%Y-%m-%d %H:%M:%S')

    if logfile:
        formatter = logging.Formatter(DEFAULT_LOGFORMAT)
        fh = logging.FileHandler(logfile)
        fh.setFormatter(formatter)
        rootLogger = logging.getLogger()
        rootLogger.addHandler(fh)
    # Set loglevel of pyvisa to INFO, otherwise the log gets jammed with too
    # many debug messages.
    if loglevel == logging.DEBUG:
        logging.getLogger('pyvisa').setLevel(logging.INFO)


def setup_awg(visa_addr):
    """Sets up the AWG and returns the AWGController instance."""
    awg = AWGController(visa_addr)
    awg.reset()
    awg.driver.trace[0].dwidth = 'precision'
    awg.configure_marker(sample_low=0, sample_high=1.6,
                         sync_low=0, sync_high=1.6)
    awg.configure_trigger(level=1.0, impedance='low', slope='rising',
                          source='external', mode='triggered')
    awg.sample_rate = 8e9
    return awg

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='awgcontrol',
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-v', '--visa-address',
                        default='TCPIP0::localhost::inst0::INSTR',
                        help='VISA address of the M8190A AWG')
    parser.add_argument('-a', '--address', default='127.0.0.1',
                        help='address where the server binds to')
    parser.add_argument('-p', '--port', default=10000, type=int,
                        help='port to listen for connections')
    parser.add_argument('-l', '--loglevel', default='INFO',
                        help='set the loglevel (DEBUG, INFO, WARNING, ERROR)')
    parser.add_argument('-f', '--logfile', default=None, type=str,
                        help='log to file (default: off)')
    parser.add_argument('--version', action='version', 
                        version='%(prog)s 0.9')
    args = parser.parse_args()

    setup_logging(logging.getLevelName(args.loglevel), args.logfile)
    awg = setup_awg(args.visa_address)
    start_server(awg, args.address, args.port)

